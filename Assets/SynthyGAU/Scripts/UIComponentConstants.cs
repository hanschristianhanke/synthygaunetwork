﻿using UnityEngine;
using System.Collections;

public class UIComponentConstants {

    public static string[] Button_Bank = new string[] { "Button_Bank_A", "Button_Bank_B", "Button_Bank_C", "Button_Bank_D" };
    public static string[] Button_Pattern = new string[] { "Button_Pattern_1", "Button_Pattern_2", "Button_Pattern_3", "Button_Pattern_4", "Button_Pattern_5", "Button_Pattern_6", "Button_Pattern_7", "Button_Pattern_8" };
    public static string[] Button_Range = new string[] { "Button_Range_16", "Button_Range_32", "Button_Range_48", "Button_Range_64" };
    public static string[] Button_Step = new string[] { "Button_Step_1_16", "Button_Step_17_32", "Button_Step_33_48", "Button_Step_49_64" };
    public static string[] Button_Edit = new string[] { "Button_Edit_Copy", "Button_Edit_Clear" };
    public static string[] Button_Control = new string[] { "Button_Control_Play", "Button_Control_Pause", "Button_Control_Stop" };
    public static string[] Button_Bpm_Control = new string[] { "Button_Control_Plus", "Button_Control_Minus" };

    public static string[] Button_EditMode = new string[] { "Button_EditMode_0", "Button_EditMode_1", "Button_EditMode_2", "Button_EditMode_3", "Button_EditMode_4", "Button_EditMode_5", "Button_EditMode_6", "Button_EditMode_7" };

    public static string[] SoundSelector = new string[] { "SoundSelector_0", "SoundSelector_1", "SoundSelector_2", "SoundSelector_3", "SoundSelector_4", "SoundSelector_5", "SoundSelector_6", "SoundSelector_7", "SoundSelector" };
    public static string[] TrackSelector = new string[] { "TrackSelector_0", "TrackSelector_1", "TrackSelector_2", "TrackSelector_3", "TrackSelector_4", "TrackSelector_5", "TrackSelector_6", "TrackSelector_7" };

    public static string[] Slider = new string[] { "Slider_Volume", "Slider_Pan", "Piano_Volume" };
    public static string[] Button_Octave = new string[] { "Button_Octave_0", "Button_Octave_1", "Button_Octave_2", "Button_Octave_3", "Button_Octave_4", "Button_Octave_5" };

    public static string[] Button_PianoWhite = new string[] { "Button_PianoWhite_0", "Button_PianoWhite_1", "Button_PianoWhite_2", "Button_PianoWhite_3", "Button_PianoWhite_4", "Button_PianoWhite_5", "Button_PianoWhite_6", "Button_PianoWhite_7", "Button_PianoWhite_8", "Button_PianoWhite_9", "Button_PianoWhite_10", "Button_PianoWhite_11", "Button_PianoWhite_12", "Button_PianoWhite_13" };
    public static string[] Button_PianoBlack = new string[] { "Button_PianoBlack_0", "Button_PianoBlack_1", "Button_PianoBlack_2", "Button_PianoBlack_3", "Button_PianoBlack_4", "Button_PianoBlack_5", "Button_PianoBlack_6", "Button_PianoBlack_7", "Button_PianoBlack_8", "Button_PianoBlack_9"};
    public static string[] Button_Harmony = new string[] { "Button_Harmony_0", "Button_Harmony_1", "Button_Harmony_2", "Button_Harmony_3", "Button_Harmony_4", "Button_Harmony_5", "Button_Harmony_6", "Button_Harmony_7"};

    public static string[] Button_Screen = new string[] { "Button_Screen_Synthy", "Button_Screen_Sampler", "Button_Screen_Setup", "Button_Screen_Sequencer", "Button_Screen_Arrangement", "Button_Screen_Effect" };
    public static string[] Button_Setup = new string[] { "Button_Save", "Button_Load", "Button_Themes" };
    public static string[] Button_Color = new string[] { "Color_Background", "Color_ButtonOff", "Color_ButtonOn", "Color_StepOff", "Color_StepOn", "Color_Piano", "Color_Label" };
    public static string[] Color_Slider = new string[] { "Slider_Red", "Slider_Green", "Slider_Blue" };

}
