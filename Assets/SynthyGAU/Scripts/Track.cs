﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;

public class Track : MonoBehaviour {

	public AudioSource source;
	public AudioChorusFilter chorusFilter;
	public AudioDistortionFilter disortationFilter;
	public AudioEchoFilter echoFilter;
	public AudioHighPassFilter highPassFilter;
	public AudioLowPassFilter lowPassFilter;
	public AudioReverbFilter reverbFilter;

	//public AudioClip audioClip;
	//public AudioClip audioClip2;

	private AudioClip audioClipTarget;


	// BIG TEST

	public AudioClip voice;
	public string voiceName;
	public string catname;

	private float[] voiceSamples;

	private int muted = 1;
	private int channels = 1;

	private int voiceSlot = 0;


	private int seconds = 8;
	private int sampleRate = 44100;

	//private int startSample = 0;
	//private int endSample = 0;

	private Dictionary<float, float[]> toneDictionary = new Dictionary<float, float[]> ();


	public bool MutedVoice
	{
		get { return muted == 0; }
		set { muted = value?0:1; }
	}

	private Note [] notes = new Note[10];

	/*
	public void playNote (float pitch){
		notes[voiceSlot] = new Note (voice.samples, pitch, voice.channels);
		voiceSlot = voiceSlot >= 9 ? 0:voiceSlot+1;
	}*/

	/*
	public void addNote (int step, float pitch){
		float [] add = new float[(int)(voiceSamples.Length/pitch)];
		source.clip.GetData (add, step * (sampleRate * seconds / 16));
		for (int i=0; i<add.Length; i++) {
			add[i] = add[i] + voiceSamples[(int)(i*pitch)];
		}
		source.clip.SetData (add, step * (sampleRate * seconds / 16));
	}
	*/

	public void addNote (int step, float pitch){
		float [] pitchedSamples;

		if (toneDictionary.ContainsKey (pitch)) {
			pitchedSamples = toneDictionary[pitch];
		} else {
			float [] pitched = new float[(int)(voiceSamples.Length/pitch)];
			for (int i=0; i<pitched.Length; i++) {
				pitched[i] = voiceSamples[(int)(i*pitch)];
			}
			toneDictionary.Add(pitch, pitched);
			pitchedSamples = pitched;
		}

		float [] add = new float[pitchedSamples.Length];
		source.clip.GetData (add, step * (sampleRate * seconds / 16));

		Debug.Log(pitchedSamples[10]+"   "+add[10]);
		Debug.Log(pitchedSamples[110]+"   "+add[110]);
		Debug.Log(pitchedSamples[210]+"   "+add[210]);
		          for (int i=0; i<pitchedSamples.Length; i++) {
			add[i] = add[i] + pitchedSamples[i];
			//Debug.Log(pitchedSamples[i]);
		}
		Debug.Log ("####");
		Debug.Log(pitchedSamples[10]+"   "+add[10]);
		Debug.Log(pitchedSamples[110]+"   "+add[110]);
		Debug.Log(pitchedSamples[210]+"   "+add[210]);

		source.clip.SetData (add, step * (sampleRate * seconds / 16));
	}

	/*
	public void removeNote (int step, float pitch){
		float [] remove = new float[(int)(voiceSamples.Length/pitch)];
		source.clip.GetData (remove, step * (sampleRate * seconds / 16));
		for (int i=0; i<remove.Length; i++) {
			remove[i] = remove[i] - voiceSamples[(int)(i*pitch)];
		}
		source.clip.SetData (remove, step * (sampleRate * seconds / 16));
	}
	*/
	/*
	public void removeNote (int step, float pitch){
		float [] remove = new float[voiceSamples.Length];
		source.clip.GetData (remove, step * (sampleRate * seconds / 16));

		float [] pitchedSamples = new float[voiceSamples.Length];
		voiceSamples.CopyTo(pitchedSamples, 0);
		PitchShifter.PitchShift (pitch*2, pitchedSamples.Length, sampleRate, pitchedSamples);

		for (int i=0; i<pitchedSamples.Length; i++) {
			remove[i] = remove[i] -  pitchedSamples[i];
		}
		source.clip.SetData (remove, step * (sampleRate * seconds / 16));
	}*/
	public void removeNote (int step, float pitch){

	
		/*float [] pitchedSamples = new float[voiceSamples.Length];
		voiceSamples.CopyTo(pitchedSamples, 0);
		PitchShifter.PitchShift (pitch*2, pitchedSamples.Length, sampleRate, pitchedSamples);
		*/
	
		//	System.IO.File.WriteAllBytes (Application.dataPath + "/Resources/Sounds/Pitched/"+categoryName+"_"+filename+"_"+i+".txt", byteArray);
	//	AudioClip clip = (AudioClip)Resources.Load ("Sounds/Pitched/" + catname + "_" + voiceName + "_" + pitch);
		//	Debug.Log ("Load " + "/Resources/Sounds/Pitched/" + catname + "_" + voiceName + "_" + pitch);

		float [] pitchedSamples = toneDictionary [pitch];
		float [] add = new float[pitchedSamples.Length];
		source.clip.GetData (add, step * (sampleRate * seconds / 16));
	
		Debug.Log ("äääääääääää");
		Debug.Log(pitchedSamples[10]+"   "+add[10]);
		Debug.Log(pitchedSamples[110]+"   "+add[110]);
		Debug.Log(pitchedSamples[210]+"   "+add[210]);
		for (int i=0; i<pitchedSamples.Length; i++) {
			add [i] = add [i] - pitchedSamples [i];
		//	
		}
	
		source.clip.SetData (add, step * (sampleRate * seconds / 16));
	}

	public void clear (){
		float [] empty = new float[sampleRate * seconds];
		for (int i = 0; i< empty.Length; i++) {
			empty[i] = 0;		
		}
		source.clip.SetData (empty, 0);
	}

	public void changeVoice (AudioClip newVoice){
		voiceSamples = new float[newVoice.samples];
		newVoice.GetData (voiceSamples, 0);
		voice = newVoice;
	}

	// Use this for initialization
	void Start () {

		muted = 1;

		for (int i=0; i<notes.Length; i++) {
			notes[i] = new Note(0,0,1);		
		}

		voiceSamples = new float[voice.samples];
		voice.GetData (voiceSamples, 0);
		channels = voice.channels;

//		Debug.Log ("voice " + voiceSamples.Length);
		source = this.gameObject.AddComponent<AudioSource> ();
		//source.volume = 0.5f;
		audioClipTarget = AudioClip.Create ("test", sampleRate*seconds, 1, voice.frequency, true, false);

		source.clip = audioClipTarget;
		source.loop = true;

		chorusFilter = this.gameObject.AddComponent<AudioChorusFilter> ();
		disortationFilter = this.gameObject.AddComponent<AudioDistortionFilter> ();
		echoFilter = this.gameObject.AddComponent<AudioEchoFilter> ();
		highPassFilter = this.gameObject.AddComponent<AudioHighPassFilter> ();
		lowPassFilter = this.gameObject.AddComponent<AudioLowPassFilter> ();
		reverbFilter = this.gameObject.AddComponent<AudioReverbFilter> ();

		chorusFilter.enabled = false;
		disortationFilter.enabled = false;
		echoFilter.enabled = false;
		highPassFilter.enabled = false;
		lowPassFilter.enabled = false;
		reverbFilter.enabled = false;
		source.priority = 255;
		//source.Play ();
	}
	
	// Update is called once per frame
	void Update () {

	}
/*
	void OnAudioFilterRead (float[] data, int channels)
		//void OnAudioRead(float[] data)
	{
		for (var i = 0; i < data.Length; i=i+2) {
			data[i] = 0;
			//int gain = 1;
			//gain = notes.Count;
			
			for (int ii=0; ii<10; ii++)
			{
				Note currentNote = notes[ii];
				
				int index = currentNote.nextSampleIndex();
				if (index < voiceSamples.Length-1){
					data[i] += voiceSamples[index]; // * muted;
					data[i+1] = data[i];
				} 
			}
			
			
			//data[i] = data [i] / gain;
			//data[i+1] = data [i+1] / gain;
		}
	}
*/
	void OnAudioSelectPosition(int newPosition)
	{
	}

}
