﻿using UnityEngine;
using System.Collections;
using System;

public class PianoUtil{

    //private static int[] bwhiteValues = new int[] {0,2,4,5,7,9,11,12,14,16,17,19,21,23};
    //private static int[] bblackValues = new int[] {1,3,6,8,10,13,15,18,20,22 };

    private static int[] bwhiteValues = new int[] { 0, 2, 4, 5, 7, 9, 11, 0, 2, 4, 5, 7, 9, 11};
    private static int[] bblackValues = new int[] { 1, 3, 6, 8, 10, 1, 3, 6, 8, 10 };

    public static int calculateToneValueForKeybordButton(String buttonName, int firstOctave, int secondOctave)
    {
        Debug.Log(buttonName);
        int bwhiteID = Array.IndexOf(UIComponentConstants.Button_PianoWhite,buttonName);
        if (bwhiteID != -1)
        {
            int adder = ((bwhiteID < 7) ? firstOctave : secondOctave) * 12;
            return bwhiteValues[bwhiteID] + adder;
        }

        int bblackID = Array.IndexOf(UIComponentConstants.Button_PianoBlack,buttonName);
        if (bblackID != -1)
        {
            int adder = ((bwhiteID < 5) ? firstOctave : secondOctave) * 12;
            return bblackValues[bblackID] + adder;
        }
        return 0;
    }

}
