﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.IO;

public class RessourceDataModel {

    private Hashtable instrumentCategories = new Hashtable();

    public string[] getInstrumentCategoryies()
    {
        ICollection keys = instrumentCategories.Keys;
        string[] array = new string[keys.Count];
        keys.CopyTo(array, 0);
        return array;
    }

    public Instrument[] getInstrumentsFromCategory(string category)
    {
        List<Instrument> instruments = (List<Instrument>) instrumentCategories[category];
        Instrument[] array = new Instrument[instruments.Count];
        instruments.CopyTo(array, 0);
        return array;
    }

    public string getCategoryForInstrument(Instrument instrument)
    {
        string[] cathegories = getInstrumentCategoryies();
        foreach (string cathegorie in cathegories)
        {
            List<Instrument> lst = (List<Instrument>) instrumentCategories[cathegorie];
            if (lst.Contains(instrument)) return cathegorie;
        }
        return null;
    }

	// MEINS
	public int getInstrumentNumberForInstrument(Instrument instrument)
	{
		int a = 0;
		string[] cathegories = getInstrumentCategoryies();
		foreach (string cathegorie in cathegories)
		{
			List<Instrument> lst = (List<Instrument>) instrumentCategories[cathegorie];
			if (lst.Contains(instrument)) return (lst.IndexOf(instrument)+a);
			a+=10;
		}
		return 0;
	}


    public RessourceDataModel()
    {
        init();
    }

    private void init(){
		/*
		instrumentCategories.Add("Solo", new List<Instrument> { new Instrument("Cello C2",120), new Instrument("Contra C2",120), new Instrument("Trumpet C2",120), new Instrument("Viola C2",120), new Instrument("Violin C3",120) });
		instrumentCategories.Add("Snare Drum", new List<Instrument> { new Instrument("SNARE",121), new Instrument("SHAKE1",122), new Instrument("SHAKE2",122), new Instrument("RIM",122), new Instrument("RIM 1",122) });
		instrumentCategories.Add("Hi Hat", new List<Instrument> { new Instrument("OP_HAT",117), new Instrument("KICK",118), new Instrument("CL_HAT",114), new Instrument("CLAP",122), new Instrument("CLAPS",122) });
		instrumentCategories.Add("Toms", new List<Instrument> { new Instrument("TOM1",126), new Instrument("TOM2",126), new Instrument("COWBELL",122) });
		instrumentCategories.Add("Percussions", new List<Instrument> { new Instrument("CLAVES",123), new Instrument("CRASH",125), new Instrument("CRASH 1",111) });   
		*/
	//	Debug.Log ("###########");
		addSubCategory ("CR8K");
		addSubCategory ("CYMBA");
		addSubCategory ("REKKERD");
		addSubCategory ("BASEDRUM");
		addSubCategory ("UNIVOX");

    }	

	private void addSubCategory (string categoryName){
//		Debug.Log ("########### "+categoryName);
		Object [] objects = Resources.LoadAll("Sounds/"+categoryName);
		List<Instrument> catInstruments = new List<Instrument> ();

		foreach (Object file in objects) {
			if (!file.name.Contains("_pitched")){
				catInstruments.Add(new Instrument(file.name, "Sounds/"+categoryName+"/"+file.name, 0, categoryName));
			//	Debug.Log("Cat "+categoryName+" -- "+file.name);
			}
		}
		instrumentCategories.Add (categoryName, catInstruments);
		objects = null;
	}
}
