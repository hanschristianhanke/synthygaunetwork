﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class ApplicationDataModel {

	public ApplicationDataModel(){
        _ressourceDataModel = new RessourceDataModel();
        _audioDataModel = new AudioDataModel(_ressourceDataModel);
        _colorDataModel = new ColorDataModel();

        init();
	}

    private void init()
    {
       
    }

    //---------------

    private RessourceDataModel _ressourceDataModel;
    public RessourceDataModel ressourceDataModel
    {
        get { return this._ressourceDataModel; }
    }

    private AudioDataModel _audioDataModel;
    public AudioDataModel audioDataModel
    {
        get { return this._audioDataModel; }
    }

    private ColorDataModel _colorDataModel;
    public ColorDataModel colorDataModel
    {
        get { return this._colorDataModel; }
    }

    //---------------

	Hashtable booleanValues = new Hashtable();
	public void setBooleanValue(String key, bool value){
		if (!booleanValues.ContainsKey(key)){
			booleanValues.Add(key,value);
			return;
		}
		booleanValues[key]=value;
	}
	public bool getBooleanValue(String key){
		if (!booleanValues.ContainsKey(key)){
			setBooleanValue(key,false);
		}
		return (bool) booleanValues[key]; 
	}
	
	//---------------
	
	Hashtable floatValues = new Hashtable();
	public void setFloatValue(String key, float value){
		if (!floatValues.ContainsKey(key)){
			floatValues.Add(key,value);
			return;
		}
		floatValues[key]=value;
	}
	public float getFloatValue(String key){
		if (!floatValues.ContainsKey(key)){
			setFloatValue(key,0);
		}
		return (float) floatValues[key]; 
	}
	//----------------
	
	Hashtable intValues = new Hashtable();
	public void setIntValue(String key, int value){
		if (!intValues.ContainsKey(key)){
			intValues.Add(key,value);
			return;
		}
		intValues[key]=value;
	}
	public int getIntValue(String key){
		if (!intValues.ContainsKey(key)){
			setIntValue(key,0);
		}
		return (int) intValues[key]; 
	}
	
	
	//----------------
	
	private Playmode _playomode = Playmode.STOP;
	public Playmode playmode 
	{
    	get { return this._playomode; }
    	set { this._playomode = value; }
	}
	
	public enum Playmode{
		PLAY, STOP, PAUSE
	}

    

    private bool _follow=false;
    public bool follow
    {
        get { return this._follow; }
        set { this._follow = value; }
    }


    private bool _copymode = false;
    public bool copymode
    {
        get { return this._copymode; }
        set { this._copymode = value; }
    }

    private bool _clearmode = false;
    public bool clearmode
    {
        get { return this._clearmode; }
        set { this._clearmode = value; }
    }

    //----------------

    private ScreenManagerView.AvailableScreens _currentScreen = ScreenManagerView.AvailableScreens.sequencer;
    public ScreenManagerView.AvailableScreens currentScreen
    {
        get { return this._currentScreen; }
        set { this._currentScreen = value; }
    }

    //----------------

    private bool _showMenu;
    public bool showMenu
    {
        get { return this._showMenu; }
        set { this._showMenu = value; }
    }

    private int _selectedmenubutton = 3;
    public int selectedmenubutton
    {
        get { return this._selectedmenubutton; }
        set { this._selectedmenubutton = value; }
    }

    private int _selectedsetupbutton = 2;
    public int selectedsetupbutton
    {
        get { return this._selectedsetupbutton; }
        set { this._selectedsetupbutton = value; }
    }


}
