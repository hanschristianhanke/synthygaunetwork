﻿using UnityEngine;
using System.Collections;

public class TrackData  {

    private const int COLUMNCOUNT = 64;
    ToneColumn[] toneColumns;

    public TrackData(){
        toneColumns = new ToneColumn[COLUMNCOUNT];
        for (int columnIndex = 0; columnIndex < COLUMNCOUNT; columnIndex++)
        {
            toneColumns[columnIndex] = new ToneColumn();
        }
    }

    public ToneColumn getToneColumn(int x)
    {
        return toneColumns[x];
    }

    public bool[,] getToneValues()
    {
        bool[,] returnVal = new bool[COLUMNCOUNT,72];
        for (int x = 0; x < COLUMNCOUNT; x++ )
        {
            ToneColumn toneColumn = toneColumns[x];
            for (int y = 0; y < 72; y++)
            {
                returnVal[x, y] = toneColumn.getToneActive(y);
            }
        }
    
        return returnVal;
    }

    public void set(TrackData trackData)
    {
        for (int i = 0; i < COLUMNCOUNT; i++)
        {
            toneColumns[i].set(trackData.getToneColumn(i));
        }
    }

    public void transposeDown()
    {

        foreach (ToneColumn toneColumn in toneColumns)
        {
            toneColumn.transposeDown();
        }
    }

    public void transposeUp()
    {
		Debug.Log ("transposeUp " + Time.fixedTime);
        foreach (ToneColumn toneColumn in toneColumns)
        {
            toneColumn.transposeUp();
        }
    }

    public void transposeStepForward()
    {
        ToneColumn toneColumn = new ToneColumn();
        toneColumn.set(toneColumns[COLUMNCOUNT-1]);
        for (int i = COLUMNCOUNT - 1; i > 0; i--)
        {
            toneColumns[i].set(toneColumns[i -1]);
            toneColumns[i].reloadPlayedTones();
        }
        toneColumns[0].set(toneColumn);
        toneColumns[0].reloadPlayedTones();
    }

    public void transposeStepBackward()
    {
        ToneColumn toneColumn = new ToneColumn();
        toneColumn.set(toneColumns[0]);
        for (int i = 0; i < COLUMNCOUNT - 1; i++)
        {
            toneColumns[i].set(toneColumns[i + 1]);
            toneColumns[i].reloadPlayedTones();
        }
        toneColumns[COLUMNCOUNT - 1].set(toneColumn);
        toneColumns[COLUMNCOUNT - 1].reloadPlayedTones();
    }
}
