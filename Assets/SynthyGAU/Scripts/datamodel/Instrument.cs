﻿using UnityEngine;
using System;
using System.Collections;

public class Instrument  {

    public Instrument(String name, int number)
    {
        this._name = name;
		this._number = number;
    }

	public Instrument(String name, String path, int number, string category)
	{
		this._name = name;
		this._number = number;
		this._path = path;
		this._category = category;
	}


    private string _name;
    public string name
    {
        get { return this._name; }
        set { this._name = value; }
    }

	private string _category;
	public string category
	{
		get { return this._category; }
		set { this._category = value; }
	}

    public override string ToString()
    {
        return _name;
    }

	private int _number;
	public int number
	{
		get { return this._number; }
		set { this._number = value; }
	}

	private string _path;
	public string path
	{
		get { return this._path; }
		set { this._path = value; }
	}
}
