﻿using UnityEngine;
using System.Collections;

public class Tone
{

    private bool _active = false;

    public bool active
    {
        get { return this._active; }
        set { this._active = value; }
    }

    private int _id;

    public int id
    {
        get { return this._id; }
    }

    public Tone(int id)
    {
        _id = id;
    }

    public void clear()
    {
        _active = false;
    }

    public void set(Tone tone)
    {
        _active = tone.active;
    }
}
