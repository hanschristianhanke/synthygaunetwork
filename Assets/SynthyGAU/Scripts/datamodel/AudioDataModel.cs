﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AudioDataModel
{
    public static int[][] harmonies = new int[][] { new int[]{1,4,6,10}, new int[]{1,4,7,11},
													new int[]{1,4,7,11}, new int[]{1,4,8,12},
													new int[]{1,5,6,11}, new int[]{1,5,8,11},
													new int[]{1,5,7,12}, new int[]{1,5,9,1}};
    
    private const int TRACKCOUNT = 8;
    private const int PATTERCOUNT = 8;
    private const int BANKCOUNT = 4;

    private Instrument[] trackInstruments = new Instrument[TRACKCOUNT+1];
    private RessourceDataModel _ressourceDataModel;
    private TrackData[, ,] audioData;
    



    // -----------------------------

    public AudioDataModel(RessourceDataModel ressourceDataModel)
    {
        init(ressourceDataModel);
    }

    private void init(RessourceDataModel ressourceDataModel)
    {
        string[] cats = ressourceDataModel.getInstrumentCategoryies();
        trackInstruments[0] = ressourceDataModel.getInstrumentsFromCategory(cats[0])[2];
        trackInstruments[1] = ressourceDataModel.getInstrumentsFromCategory(cats[0])[0];
        trackInstruments[2] = ressourceDataModel.getInstrumentsFromCategory(cats[1])[0];
        trackInstruments[3] = ressourceDataModel.getInstrumentsFromCategory(cats[3])[1];
        trackInstruments[4] = ressourceDataModel.getInstrumentsFromCategory(cats[4])[0];
        trackInstruments[5] = ressourceDataModel.getInstrumentsFromCategory(cats[3])[1];
        trackInstruments[6] = ressourceDataModel.getInstrumentsFromCategory(cats[4])[1];
        trackInstruments[7] = ressourceDataModel.getInstrumentsFromCategory(cats[4])[2];
//        trackInstruments[8] = ressourceDataModel.getInstrumentsFromCategory(cats[2])[3];

        audioData = new TrackData[BANKCOUNT, PATTERCOUNT, TRACKCOUNT];
        for (int bankIndex = 0; bankIndex < BANKCOUNT; bankIndex++)
        {
            for (int patternIndex = 0; patternIndex < PATTERCOUNT; patternIndex++)
            {
                for (int trackIndex = 0; trackIndex < TRACKCOUNT; trackIndex++)
                {
                    audioData[bankIndex, patternIndex, trackIndex] = new TrackData();
                }
            }
        }

        // volumes
        for (int i = 0;i <TRACKCOUNT;i++){
            trackVolumes[i] = 0.8f;
            panValues[i] = 0.5f;
        }


    }

    // -----------------------------

	// MEINS

	public int getInstrumentNumberNyName (Instrument name){
		return _ressourceDataModel.getInstrumentNumberForInstrument (name);
	}

     public ToneColumn getToneColumn(int bankIndex, int patternIndex, int trackIndex, int columnIndex)
     {
         return audioData[bankIndex, patternIndex, trackIndex].getToneColumn(getCurrentXValue(columnIndex));
     }

     public TrackData getGetDataFromCurrentTrack()
     {
         return audioData[_bank, _pattern, _track];
     }

     public ToneColumn getToneColumnFromCurrentTrack(int track, int x)
     {
         //if (_track == -1) return null;
         return audioData[_bank, _pattern, track].getToneColumn(x);
     }

    public void setCurrentToneActive(int x, int y, bool active)
    {
        if (_track == -1)
        {
            if (active) { 
				audioData[_bank, _pattern, y].getToneColumn(getCurrentXValue(x)).setDefaultTone(); 
				_tracks[y].addNote(x, 1);
			}
            else { audioData[_bank, _pattern, y].getToneColumn(getCurrentXValue(x)).clear();
				_tracks[y].removeNote(x, 1);
			}
        }
        else {
            //Debug.Log(_bank + " " + _pattern + " " + _track + " " + getCurrentXValue(x) + " " + getCurrentYValue(y));            
            audioData[_bank, _pattern, _track].getToneColumn(getCurrentXValue(x)).setToneActive(getCurrentYValue(y), active); 
			//float pitch = ((float)(audioData[_bank, _pattern, _track].getToneColumn(getCurrentXValue(x)).getToneRange() - getCurrentYValue(y))/(float)(audioData[_bank, _pattern, _track].getToneColumn(getCurrentXValue(x)).getToneRange()-audioData[_bank, _pattern, _track].getToneColumn(getCurrentXValue(x)).getDefaultTone()));
			//float pitch = ((float)(getCurrentYValue(y))/(float)audioData[_bank, _pattern, _track].getToneColumn(getCurrentXValue(x)).getDefaultTone());

			float pitch = Mathf.Pow(2, ( (getCurrentYValue(y)-35f)/12f));

			//Debug.Log ("track "+_track+" hoehe "+getCurrentYValue(y)+" p "+pitch+" set "+active);
			if (active) { 
				audioData[_bank, _pattern, y].getToneColumn(getCurrentXValue(x)).setDefaultTone(); 
				_tracks[_track].addNote(x, pitch);
			}
			else { audioData[_bank, _pattern, y].getToneColumn(getCurrentXValue(x)).clear();
				_tracks[_track].removeNote(x, pitch);
			}
		}
    }

    public bool getCurrentToneActive(int x, int y)
    {
        if (_track == -1)
        { return !audioData[_bank, _pattern, y].getToneColumn(getCurrentXValue(x)).nothingToPlay(); }
        else { return audioData[_bank, _pattern, _track].getToneColumn(getCurrentXValue(x)).getToneActive(getCurrentYValue(y)); }
    }



    public bool[,] getCurrentToneActiveValues()
    {
        //int hightVal = 8 * _selectedhight;
        bool[,] returnValue = new bool[16, 8];
        for (int x = 0; x < 16; x++)
        {
            for (int y = 0; y < 8; y++)
            {
                returnValue[x, y] = getCurrentToneActive(x, y);
            }
        }
        return returnValue;
    }

    

    private int getCurrentXValue(int x)
    {
        return (_selectedrange * 16) + x;
    }

    private int getCurrentYValue(int y)
    {
        return (_selectedhight * 8) + y;
    }

    // ----------------------------- Clear STUFF

    public void clearPattern(int bankIndex, int patternIndex)
    {
        for (int trackIndex = 0; trackIndex < TRACKCOUNT; trackIndex++)
        {
            for (int columnIndex = 0; columnIndex < 64; columnIndex++)
            {
                
                audioData[bankIndex, patternIndex, trackIndex].getToneColumn(columnIndex).clear();
            }
        }
    }
    public void clearBank(int bankIndex)
    {
        for (int patternIndex = 0; patternIndex < PATTERCOUNT; patternIndex++)
        {
            clearPattern(bankIndex, patternIndex);
        }
    }

    // ----------------------------- Copy Stuff

    private void copyPattern(int fromBankIndex, int toBankIndex, int fromPatternIndex, int toPatternIndex)
    {
        for (int trackIndex = 0; trackIndex < TRACKCOUNT; trackIndex++)
        {
            audioData[toBankIndex, toPatternIndex, trackIndex].set(audioData[fromBankIndex, fromPatternIndex, trackIndex]);
        }
    }
    public void copyPattern(int bankIndex, int fromPatternIndex, int toPatternIndex)
    {
        copyPattern(bankIndex, bankIndex, fromPatternIndex, toPatternIndex);
    }

    public void copyBank(int fromBank, int toBank)
    {
        for (int i = 0; i < PATTERCOUNT; i++)
        {
            copyPattern(fromBank, toBank, i, i);
        }     
    }
   
    // -----------------------------


    public Instrument getTrackInstrument(int i)
    {
        return trackInstruments[i];
    }
    public void setTrackInstument(int i, Instrument instrument)
    {
        trackInstruments[i] = instrument;
		_tracks[i].clear();
		updateTrackInstrument (i, instrument);
		for (int s=0; s<16; s++){
			List <Tone> playedTones = audioData[_bank, _pattern, i].getToneColumn(s)._playedTones;
			foreach (Tone thisTone in playedTones){
				//float pitch = ((float)(audioData[_bank, _pattern, i].getToneColumn(getCurrentXValue(s)).getToneRange() - thisTone.id)/(float)(audioData[_bank, _pattern, i].getToneColumn(getCurrentXValue(s)).getToneRange()-audioData[_bank, _pattern, i].getToneColumn(getCurrentXValue(s)).getDefaultTone()));
				_tracks[i].addNote(s, thisTone.id);
			}
		}
    }

    //--------------------------------

    private int _step = 0;
    public int step
    {
        get { return this._step; }
        set { this._step = value; }
    }

    private int _bpm = 120;
    public int bpm
    {
        get { return this._bpm; }
        set { this._bpm = value; }
    }

    // -----------------------------

    private float _mainVolume = 0.8f;
    public float mainVolume
    {
        get { return this._mainVolume; }
        set { this._mainVolume = value; }
    }

    private float _pianoVolume = 0.8f;
    public float pianoVolume
    {
        get { return this._pianoVolume; }
        set { this._pianoVolume = value; }
    }

    private float[] trackVolumes = new float[TRACKCOUNT];
    public float getTackVolume(int i)
    {
        return trackVolumes[i];
    }

    public void setTackVolume(int i, float value)
    {
        trackVolumes[i] = value;
    }

    // -----------------------------

    private float _mainPan = 0.5f;
    public float mainPan
    {
        get { return this._mainPan; }
        set { this._mainPan = value; }
    }

    private float[] panValues = new float[TRACKCOUNT];
    public float getPanValue(int i)
    {
        return panValues[i];
    }

    public void setPanValue(int i, float value)
    {
        panValues[i] = value;
    }

    // -----------------------------

    private int _bank = 0;
    public int bank
    {
        get { return this._bank; }
        set { this._bank = value; }
    }

    private int _pattern = 0;
    public int pattern
    {
        get { return this._pattern; }
        set { this._pattern = value; }
    }

    private int _range = 0;
    public int range
    {
        get { return this._range; }
        set { this._range = value; }
    }

    // -----------------------------------

    private int _track = -1;
    public int track
    {
        get { return this._track; }
        set { this._track = value; }
    }

    private int _instrumentselection = -1;
    public int instrumentselection
    {
        get { return this._instrumentselection; }
        set { this._instrumentselection = value; }
    }

    private int _trackvolumediteselection = -1;
    public int trackvolumediteselection
    {
        get { return this._trackvolumediteselection; }
        set { this._trackvolumediteselection = value; }
    }

    // -----------------------------------

    private int _selectedrange = 0;
    public int selectedrange
    {
        get { return this._selectedrange; }
        set { this._selectedrange = value; }
    }

    private int _selectedhight = 0;
    public int selectedhight
    {
        get { return this._selectedhight; }
        set { this._selectedhight = value; }
    }

    // -----------------------------------

    private bool _fistOctaveWasSetLast = true;
    public bool fistOctaveWasSetLast
    {
        get { return this._fistOctaveWasSetLast; }
        set { this._fistOctaveWasSetLast = value; }
    }

    private int _firstOctave = 2;
    public int firstOctave
    {
        get { return this._firstOctave; }
        set
        {
            if (value < secondOctave)
            {
                this._firstOctave = value;
            }
            else
            {
                this._firstOctave = _secondOctave;
                this._secondOctave = value;
            }
        }
    }

    private int _secondOctave = 3;
    public int secondOctave
    {
        get { return this._secondOctave; }
        set {
            if (value > firstOctave)
            {
                this._secondOctave = value;
            }
            else
            {
                this._secondOctave = _firstOctave;
                _firstOctave = value;
            }
        }
    }

    // -----------------------------------

    private int _selectedharmony = -1;
    public int selectedharmony
    {
        get { return this._selectedharmony; }
        set { this._selectedharmony = value; }
    }

    public int harmonyLength
    {
        get { return (_selectedharmony == -1) ? 12 : 4; }
    } 

	private Track [] _tracks;
	public Track []  tracks
	{
		get { return this._tracks; }
		set { this._tracks = value; }
	}

	private void updateTrackInstrument(int track, Instrument instrument){
		Debug.Log ("intrument changed " + track + " --> " + instrument.name);
		_tracks [track].changeVoice((AudioClip)Resources.Load(instrument.path));
		_tracks [track].voiceName = instrument.name;
		_tracks [track].catname = instrument.category;
	}
}