﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;


public class ToneColumn
{

    private const int MAXTONES = 8;
    private const int DEFAULTVALUE = 32;
	private const int TONERANGE = 72;

    private Tone[] tones;
    public List<Tone> _playedTones;
    public List<Tone> playedTones
    {
        get { return this._playedTones; }
    }

    public ToneColumn()
    {
        tones = new Tone[72];
        _playedTones = new List<Tone>();
        for (int i = 0; i < tones.Length; i++) { tones[i] = new Tone(i); }
    }

	public int getDefaultTone (){
		return DEFAULTVALUE;
	}

	public int getToneRange(){
		return TONERANGE;
	}

    public bool getToneActive(int i)
    {
        return tones[i].active;
    }

    public void setDefaultTone()
    {
        setToneActive(DEFAULTVALUE, true);
    }

    public void setToneActive(int i, bool active)
    {
        if (_playedTones.Count >= MAXTONES) return;
        Tone tone = tones[i];
        tone.active = active;
        if (active) { _playedTones.Add(tone); }
        else { _playedTones.Remove(tone); }
    }

    public Tone[] getTones(int from, int to)
    {
        int copyCount = to - from;
        Tone[] partArray = new Tone[copyCount];
        Array.Copy(tones, from, partArray, 0, copyCount);
        return partArray;
    }

    public bool nothingToPlay()
    {
        return (_playedTones.Count == 0);
    }

    public void clear()
    {
        _playedTones.Clear();
        foreach (Tone tone in tones)
        {
            tone.clear();
        }
    }

    public void set(ToneColumn toneColumn)
    {
        clear();
        List<Tone> playedTones = toneColumn.playedTones;
        foreach (Tone tone in playedTones)
        {
            Tone thisTone = tones[tone.id];
            thisTone.set(tone);
            _playedTones.Add(thisTone);
        }
    }

    public void transposeUp(){
        int len = tones.Length;
        for (int i = 0; i < len-1; i++)
        {
            tones[i].set(tones[i + 1]);
        }
        tones[len - 1].set(new Tone(0));
        reloadPlayedTones();
    }

    public void transposeDown(){
        int len = tones.Length;
        Tone tone = new Tone(0);
       
        for (int i = len-1; i > 0 ; i--)
        {
            tones[i].set(tones[i-1]);
        }
        tones[0].set(new Tone(0));
        reloadPlayedTones();
    }

    public void reloadPlayedTones(){
        _playedTones.Clear();
        foreach (Tone tone in tones)
        {
            if (tone.active)  _playedTones.Add(tone);
        }
    }
}
