﻿using UnityEngine;
using System.Collections;
using System;

public class ColorDataModel {

    

    Hashtable colorValues = new Hashtable();
    public void setColorValue(String key, Color value)
    {
        if (!colorValues.ContainsKey(key))
        {
            colorValues.Add(key, value);
            return;
        }
        colorValues[key] = value;
    }
    public Color getColorValue(String key)
    {
        if (!colorValues.ContainsKey(key))
        {
            setColorValue(key, new Color(0, 0, 0));
        }
        return (Color)colorValues[key];
    }

    // -------------------------------------------

    private int _selectedthemecolor = 0;
    public int selectedthemecolor
    {
        get { return this._selectedthemecolor; }
        set { this._selectedthemecolor = value; }
    }

    private Color[] _themeColors=new Color[7]; 

    public Color getThemeColor(int index){
        return _themeColors[index];
    }
    public void setThemeColor(int index, Color color)
    {
        _themeColors[index] = color;
    }

}
