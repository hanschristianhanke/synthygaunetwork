﻿using UnityEngine;
using System.Collections;

public class Sequencer : AController {
	
	private float timer = 0;
	private int counter = 0;
	private int oldcounter = 0;
	private Synthesizer _synthesizer;


    public Synthesizer synthesizer
    {
        set
        { this._synthesizer = value; }
    }

    void Awake(){

    }

	public override void OnNotify(NotificationData notificationData){
		NotificationType ntype = notificationData.notificationType;
		switch(ntype){
				case NotificationType.PLAYMODEBUTTONCLICKED:
					if(applicationDataModel.playmode==ApplicationDataModel.Playmode.STOP){
                        audioDataModel.step = 0;
						timer = 0;
						_synthesizer.stopAll();
					}
				break;
		}
	}

	void FixedUpdate () {
		if (applicationDataModel==null) return;
		if(applicationDataModel.playmode!=ApplicationDataModel.Playmode.PLAY) return;

		timer += Time.fixedDeltaTime; //*5;
		if (timer >= 60f/applicationDataModel.audioDataModel.bpm) {
			counter = (counter+1)% (((audioDataModel.range * 16) + 16));
			timer = 0;
			if (oldcounter != counter)
			{
				Debug.Log("cm "+counter);
				oldcounter = counter;
				if (_synthesizer != null) _synthesizer.OnStep(counter);
				
				audioDataModel.step = counter;
				rootController.OnNotify(new NotificationData(NotificationType.STEP,gameObject,null));
			}
		}
		/*

        int newStep = (Mathf.FloorToInt(timer) % ((audioDataModel.range * 16) + 16));


        if (audioDataModel.step != newStep)
        {
            if (_synthesizer != null) _synthesizer.OnStep(newStep);

			audioDataModel.step = newStep;
			rootController.OnNotify(new NotificationData(NotificationType.STEP,gameObject,null));
		}*/

	}
}
