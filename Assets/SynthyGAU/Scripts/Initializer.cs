﻿using UnityEngine;
using System.Collections;

public class Initializer {
	
	private RootController rootController;
	private ApplicationDataModel applicationDataModel;
	
	public Initializer(RootController rootController, ApplicationDataModel applicationDataModel){
		this.rootController = rootController;
		this.applicationDataModel = applicationDataModel;
	}


	
	public void Initialize(){



        initColors();

        /*applicationDataModel.setBooleanValue(UIComponentConstants.Button_Bank[0], true);
        rootController.OnNotify(new NotificationData(AController.NotificationType.BANKBUTTONCLICKED, null, null));
		
        applicationDataModel.setBooleanValue(UIComponentConstants.Button_Pattern[0], true);
        rootController.OnNotify(new NotificationData(AController.NotificationType.PATTERNBUTTONCLICKED, null, null));

        applicationDataModel.setBooleanValue(UIComponentConstants.Button_Range[0], true);
        rootController.OnNotify(new NotificationData(AController.NotificationType.RANGEBUTTONCLICKED, null, null));
		
        applicationDataModel.setBooleanValue(UIComponentConstants.Button_Control[2], true);
		rootController.OnNotify(new NotificationData(AController.NotificationType.PLAYMODEBUTTONCLICKED, null, null ));
		
        applicationDataModel.setBooleanValue(UIComponentConstants.Button_Step[0], true);
        rootController.OnNotify(new NotificationData(AController.NotificationType.STEPBUTTONCKLICKED,null,null));*/

        GameObject go = GameObject.Find("Button_Bank_A");
        rootController.OnNotify(new NotificationData(AController.NotificationType.BANKBUTTONCLICKED, go, new StepControlDataChangeCommand(AController.NotificationType.BANKBUTTONCLICKED, "Button_Bank_A", applicationDataModel)));

        go = GameObject.Find("Button_Pattern_1");
        rootController.OnNotify(new NotificationData(AController.NotificationType.PATTERNBUTTONCLICKED, go, new StepControlDataChangeCommand(AController.NotificationType.PATTERNBUTTONCLICKED, "Button_Pattern_1", applicationDataModel)));

        go = GameObject.Find("Button_Range_16");
        rootController.OnNotify(new NotificationData(AController.NotificationType.RANGEBUTTONCLICKED, go, new StepControlDataChangeCommand(AController.NotificationType.RANGEBUTTONCLICKED, "Button_Range_16", applicationDataModel)));

        go = GameObject.Find("Button_Control_Stop");
        rootController.OnNotify(new NotificationData(AController.NotificationType.PLAYMODEBUTTONCLICKED, go, new StepControlDataChangeCommand(AController.NotificationType.PLAYMODEBUTTONCLICKED, "Button_Control_Stop", applicationDataModel)));

        go = GameObject.Find("Button_Step_1_16");
        rootController.OnNotify(new NotificationData(AController.NotificationType.STEPBUTTONCKLICKED, go, new StepMatrixDataChangeCommand(AController.NotificationType.STEPBUTTONCKLICKED, "Button_Step_1_16", applicationDataModel)));

        applicationDataModel.setBooleanValue(UIComponentConstants.Button_Octave[2], true);
        applicationDataModel.setBooleanValue(UIComponentConstants.Button_Octave[3], true);
        rootController.OnNotify(new NotificationData(AController.NotificationType.OCTAVESELECTION, null, null ));

		rootController.OnNotify(new NotificationData(AController.NotificationType.PANSLIDERDRAGGED, null,null));		
		
        rootController.OnNotify(new NotificationData(AController.NotificationType.VOLUMESLIDERDRAGGED, null, null));
        
        rootController.OnNotify(new NotificationData(AController.NotificationType.SELECTINSTRUMENT, null, null));

        applicationDataModel.setBooleanValue(UIComponentConstants.Button_Screen[3], true);
        rootController.OnNotify(new NotificationData(AController.NotificationType.MENUBUTTONCLICKED, null, null));

        rootController.OnNotify(new NotificationData(AController.NotificationType.SELECTTHEMECOLOR, null, null));
        
       // setBooleanValue(UIComponentConstants.Button_Setup[2], true); 
    }

    //public static string[] Button_Color = new string[] { "Color_Background", "Color_ButtonOff", "Color_ButtonOn", "Color_StepOff", "Color_StepOn", "Color_Piano", "Color_Label" };
    private void initColors()
    {
        ColorDataModel colorDataModel = applicationDataModel.colorDataModel;
        
        Color color = AvailableColors.Background;
        Color newColor = new Color(color.r, color.g, color.b);
        colorDataModel.setColorValue(UIComponentConstants.Button_Color[0], newColor);
        colorDataModel.setThemeColor(0, newColor);
        
        color = AvailableColors.Button_Off;
        newColor = new Color(color.r, color.g, color.b);
        colorDataModel.setColorValue(UIComponentConstants.Button_Color[1], newColor);
        colorDataModel.setThemeColor(1, newColor);

        color = AvailableColors.Button_On;
        newColor = new Color(color.r, color.g, color.b);
        colorDataModel.setColorValue(UIComponentConstants.Button_Color[2], newColor);
        colorDataModel.setThemeColor(2, newColor);

        color = AvailableColors.Step_Off;
        newColor = new Color(color.r, color.g, color.b);
        colorDataModel.setColorValue(UIComponentConstants.Button_Color[3], newColor);
        colorDataModel.setThemeColor(3, newColor);

        color = AvailableColors.Step_On;
        newColor = new Color(color.r, color.g, color.b);
        colorDataModel.setColorValue(UIComponentConstants.Button_Color[4], newColor);
        colorDataModel.setThemeColor(4, newColor);

        color = AvailableColors.Piano;
        newColor = new Color(color.r, color.g, color.b);
        colorDataModel.setColorValue(UIComponentConstants.Button_Color[5], newColor);
        colorDataModel.setThemeColor(5, newColor);

        color = AvailableColors.Text;
        newColor = new Color(color.r, color.g, color.b);
        colorDataModel.setColorValue(UIComponentConstants.Button_Color[6], newColor);
        colorDataModel.setThemeColor(6, newColor);
    }
}
