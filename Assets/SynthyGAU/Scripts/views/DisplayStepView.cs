﻿using UnityEngine;
using System.Collections;

public class DisplayStepView : ViewComponent {
	//public enum RangeType {"1-16" = 0, "17-32" = 1, "33-48" = 2, "49-64" = 3};
	
	
	private Texture2D texture;
	private int offset = 1;
	private int stepCount = 16;
	
	public int range = 0;
	private bool busy = false;

	// Use this for initialization
	void Awake () {
		//renderer.material.shader = Shader.Find("Transparent/Cutout/Soft Edge Unlit");
		renderer.material.shader = Shader.Find("Unlit/Transparent");
		redraw ();
		for (int i = 0; i<16; i++){
			drawStep(i, ColorPresets.button_off[0]);
		}

		renderer.material.mainTexture = texture;
		renderer.material.color = Color.white;
	}

	
	void drawStep (int number, Color col){
		if (!gameObject.activeInHierarchy) return;
		int pixelXSize = (int)((renderer.bounds.size.x * offset) / (stepCount)-0.5f);
		int pixelYSize = ((int)renderer.bounds.size.y - 1);
		
		for (int x = 0; x < pixelXSize; x++){
			for (int y = 1; y < pixelYSize; y++){
				texture.SetPixel (number * (pixelXSize+1) + x, y, col);
			}
		}
		texture.Apply();
	}
	
	public void setStep(int step){
		//Debug.Log (step);
		if (step >= range * stepCount && step < (range + 1) * stepCount) {
			drawStep (step % 16, AvailableColors.Button_On);
			drawStep ((step - 1) % 16, ColorPresets.button_off [0]);
			if (step%16 == 0){
				drawStep (15, ColorPresets.button_off [0]);
			}
		//	drawStep ((step + 1) % 16, ColorPresets.button_off [0]);
			busy = true;
			renderer.material.mainTexture = texture;
			renderer.material.color = Color.white;
		} else {
			if (busy){
				drawStep (15, ColorPresets.button_off [0]);
				busy = false;
				renderer.material.mainTexture = texture;
				renderer.material.color = Color.white;
			}
		}
	}

	override public void redraw(){
		texture = new Texture2D ((int)renderer.bounds.size.x, (int)renderer.bounds.size.y);		
		
		for (int x = 0; x < (int)renderer.bounds.size.x; x++){
			for (int y = 0; y < (int)renderer.bounds.size.y; y++){
				texture.SetPixel (x, y, ColorPresets.button_off[0]);
			}
		}
	}
}
