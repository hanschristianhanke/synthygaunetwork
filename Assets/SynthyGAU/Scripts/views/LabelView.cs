﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LabelView : ViewComponent {
	public enum LabelColorTypes {TypA = 0, TypB = 1};
	public LabelColorTypes labelColorTyp = LabelColorTypes.TypA;
	public string suffix = "";
	public string prefix = "";
	
	private Texture2D label; 
	private TextMesh textField;
	
	void Awake () {
		textField = gameObject.GetComponentInChildren<TextMesh>();
		Vector3 localScale = transform.localScale;
		localScale.z = 1;
		transform.localScale = localScale;
		//renderer.material.shader = Shader.Find ("Transparent/Cutout/Soft Edge Unlit");
		renderer.material.shader = Shader.Find("GUI/Text Shader");
		redraw();		

	}
	
	public void setText(string newText){
		textField.text = prefix+newText+suffix;
	}
	
	override public void redraw(){
		label = new Texture2D(1,1, TextureFormat.RGBA32, false);
		label.SetPixel(1,1, Color.white);

		renderer.material.color = ColorPresets.label[(int)labelColorTyp];
		textField.color = ColorPresets.label_text[(int)labelColorTyp];
		label.Apply();	
	}		
}
