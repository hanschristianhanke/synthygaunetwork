﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SliderView : ViewComponent {
	
	public enum SliderTypes {Pan = 0, Volume = 1, Free = 2};
	public SliderTypes sliderType = SliderTypes.Pan;
	public string caption = "";

	public Color startColor;
	public Color endColor;

	private string resName = "Theme/slider_Slider_";
	private Texture2D slider;
	//private List<GameObject> layers = new List<GameObject>();	
	private List<Texture2D> layerTextures = new List<Texture2D>();
	
	private TextMesh label;
	private TextMesh valLabel;
	
//	private float testV = 0;
//	private Camera thisCamera;	
	
//	private float minX = 0.06f;
//	private float maxX = 0.88f;
	
	private SliderController sliderController;
	
	// Use this for initialization
	void Awake () {
	//	thisCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
		sliderController = (SliderController)gameObject.GetComponent<SliderController>();
		
		slider = new Texture2D(1,1, TextureFormat.RGBA32, false);
		slider.SetPixel(1,1, Color.white);		
		//renderer.material.shader = Shader.Find ("Transparent/Cutout/Soft Edge Unlit");
		renderer.material.shader = Shader.Find("GUI/Text Shader");
		renderer.material.color = ColorPresets.slider_background;
		slider.Apply();		
		
		// 0 = Verlauf, 1 = Stopper, 2 = Outline, 3 = Knob
		newLayers(4);
		
		label = transform.Find("label").GetComponent<TextMesh>();
		valLabel = transform.Find("value").GetComponent<TextMesh>();
		
		redraw();
		setValue(0.5f);
	}
	
	void Update () {
	}
	
	override public void redraw(){

		label.text = caption.Equals("")?ColorPresets.slider_label[(int)sliderType]:caption;

		layerTextures = new List<Texture2D>();
		renderer.material.color = ColorPresets.slider_background;
		layerTextures.Add ((Texture2D)Resources.Load (resName + "2")); //, ColorPresets.slider_background));
		layerTextures.Add ((Texture2D)Resources.Load (resName + "3")); //, ColorPresets.slider_knob));
		if (sliderType != SliderTypes.Free) {
			layerTextures.Add (calculateColor ((Texture2D)Resources.Load (resName + "0"), ColorPresets.slider_color [(int)sliderType]));
		} else {
			layerTextures.Add (calculateColor ((Texture2D)Resources.Load (resName + "0"), new Color [] {startColor, endColor}));
		}
		layerTextures.Add((Texture2D)Resources.Load (resName+"1")); //, ColorPresets.slider_knob));		
		layers [2].renderer.material = new Material(Shader.Find("Unlit/Transparent"));

		for (int i = 0; i<layers.Count; i++){					
			layers[i].renderer.material.mainTexture = layerTextures[i];
			layers[i].transform.localScale = new Vector3( layerTextures[i].width / renderer.bounds.size.x, layerTextures[i].height / renderer.bounds.size.y, 0);
		}

		layers[0].renderer.material.color = ColorPresets.slider_background;
		layers [1].renderer.material.color = ColorPresets.slider_knob;

		layers[3].renderer.material.color = ColorPresets.slider_knob;
		
		layers[1].transform.parent = layers[0].transform;
		layers[1].transform.localPosition = new Vector3(0.05f, -0.05f, 0f);
		layers[1].transform.localScale = new Vector3(0.9f, 0.9f, 0f);
		valLabel.color = ColorPresets.button_font_on;
	}	
	
	public void setValue (float val){
		layers[0].transform.localPosition = new Vector3(0.06f + (0.82f*val), -0.515f, -0.5f);		
		valLabel.text = (sliderType != SliderTypes.Pan)?Mathf.FloorToInt(val*100).ToString():(Mathf.FloorToInt(val*200)-100).ToString();
	}
		
	
}
