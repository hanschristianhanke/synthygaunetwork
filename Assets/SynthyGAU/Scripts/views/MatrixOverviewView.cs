﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using TouchScript.Events;
using TouchScript.Gestures;

public class MatrixOverviewView : ViewComponent {
	private static int boxSizeX = 5;
	private static int boxSizeY = 5;
	private static int sizeX = 64;
	private static int sizeY = 72;

	private static int scaleX;
	private static int scaleY;

	private static Texture2D boxes;

	private static int counter = 0;

	public int offsetX = 0;
	public int offsetY = 0;

	private static Texture2D selectionTexture;

	/*
	private int dummyStep = 0;
	private int dummyNote = 0;

	private float deltaStep = 0;
	private float deltaNote = 0;

	private float lastMousePositionX = 0;
	private float lastMousePositionY = 0;*/



	//private static TextMesh debugText;

	private static bool[,] lastStates = new bool[64, 72];

	private static int targetX;
	private static int targetY;

	private static int harmonyLength;
	private static int height = 1;

	private static Color onColor;
	private static Color offColor;

	//private static List<GameObject>  myLayers;
	private static GameObject frame;

	private bool allowed;

	public MatrixOverviewView(){
		allowed = false;
		Debug.Log ("Here I am -------------- ");
	}

	// Use this for initialization
	void Awake () {

		allowed = true;
		Debug.Log ("AWAKE --------------");
		//gameObject.transform.localScale  = new Vector2 (sizeX * (boxSizeX + 1) + ((sizeX / 16) - 2), sizeY * (boxSizeY + 1));
		scaleX = sizeX * (boxSizeX + 1) + ((sizeX / 16) - 2); //(int)gameObject.transform.localScale.x;
		scaleY = sizeY * (boxSizeY + 1); //(int)gameObject.transform.localScale.y;
		boxes = new Texture2D((int)renderer.bounds.size.x, (int)renderer.bounds.size.y, TextureFormat.RGBA32, false);
		//renderer.material = new Material(Shader.Find("Default-Diffuse"));
		//renderer.material.shader = Shader.Find("Transparent/Cutout/Soft Edge Unlit");
		renderer.material.shader = Shader.Find("Unlit/Transparent");
		renderer.material.mainTexture = boxes;

		boxes.filterMode = FilterMode.Point;
		//renderer.material.color = ColorPresets.MatrixOverview[0];

//		Debug.Log (scaleX + " /// " + scaleY);
		for (int y = 0; y< boxes.height; y++){	
			for (int x = 0; x< boxes.width ; x++) {
				boxes.SetPixel(x,y, ColorPresets.MatrixOverview[0]);
			}
		}

		offsetX = Mathf.FloorToInt ((boxes.width - scaleX) / 2);
		offsetY = Mathf.FloorToInt ((boxes.height - scaleY) / 2);

		//newLayers (1);
		//frame = gimmeGimmeGimme ();
		//base.layers[0].renderer.material.shader = Shader.Find("Transparent/Cutout/Soft Edge Unlit");
	//	frame.renderer.material.shader = Shader.Find("Unlit/Transparent");
		//debugText = GameObject.Find ("DebugText").GetComponent<TextMesh> ();


		// KANN RAUS!!!!
		//redraw(new bool[64,72]);
		for (int x = 0; x<sizeX; x++) {
			for (int y = 0; y<sizeY/height; y++) {
				drawField (x, y, ColorPresets.MatrixOverview [1], height);
			}
		}

		bool [,] test = new bool[64,24];
		for (int x = 0; x<64; x++) {
			int num = 0;
			for (int y = 0; y<24; y++) {
				test[x,y] = Random.value<0.5f;
			}
		}

		//setViewFrame (0, 0);
		//redraw (test);
	}


	// x=step (0-63), y=note (0-71)
	public void redraw (bool [,] states){
		if (states != null) {
			/*if (frame == null){
				frame = gimmeGimmeGimme ();
			}*/

						onColor = ColorPresets.MatrixOverview [2];
						offColor = ColorPresets.MatrixOverview [1];

						offsetX = Mathf.FloorToInt ((boxes.width - scaleX) / 2);
						offsetY = Mathf.FloorToInt ((boxes.height - scaleY) / 2) + (Dummy.selectedHarmony != -1 ? 2 : 0);
						height = states.GetLength(1)==72?1:3;

		//	height = 3;
						if (lastStates.GetLength(1) != states.GetLength(1)){
							drawInit (height);
				        }

						for (int x = 0; x<sizeX; x++) {
								for (int y = 0; y<sizeY/height; y++) {
										if (states[x,y] != lastStates[x,y]){
											drawField (x, y, (states [x, y] ? onColor : offColor), height);
										}
								}
						}	

						selectionTexture = new Texture2D (1, 1, TextureFormat.RGBA32, false);

						Color frameColor = ColorPresets.MatrixOverview [3];
						frameColor.a = 0.3f;

						selectionTexture.SetPixel (1, 1, frameColor);
						selectionTexture.Apply ();

			//base.layers[0].renderer.material.shader = Shader.Find("Transparent/Cutout/Soft Edge Unlit");
		//	frame.renderer.material.shader = Shader.Find("Unlit/Transparent");
		//	frame.renderer.material.mainTexture = selectionTexture;

					//	frame.gameObject.transform.parent = this.transform;
					//	frame.gameObject.transform.localScale = new Vector3 (((boxSizeX + 1) * 16) / this.transform.localScale.x, ((boxSizeY + 1) * 8 * height) / this.transform.localScale.y, 0f);
						//setViewFrame (0, 0);
				}
		lastStates = states;
	}

	// x=step (0-63), y=note (0-71)
	public void redraw (int step, int note, bool [,] states){
		onColor = ColorPresets.MatrixOverview [2];
		offColor = ColorPresets.MatrixOverview [1];

		//	octaveNote = note%12;

			height = states.GetLength(1)==72?1:3;	
	//	height = 3;
			if (lastStates.GetLength(1) != states.GetLength(1)){
				drawInit (height);
			}

			if (states[step,note] != lastStates[step, note]){
				drawField (step, note, (states [step, note]?onColor:offColor), height );
			}
					
		lastStates = states;
	}

	public override void redraw(){
		redraw (lastStates);
	}


	public void setViewFrame (int step, int note){

		/*int sizeYFrame;
		int beginYFrame;

		sizeYFrame = 8;

		beginYFrame = Mathf.Clamp(note, 0, (sizeY/height)-sizeYFrame);
		step = Mathf.Clamp (step, 0, 48);
		drawFrame (beginYFrame, sizeYFrame, step);*/
	}

	private void drawFrame (int beginYFrame, int sizeYFrame, int step){
		int yy = (boxSizeY+1)*height*beginYFrame;
		int xx = (boxSizeX+1)*step+ Mathf.FloorToInt(step/16);
		if (frame != null) {
			frame.gameObject.transform.localPosition = new Vector3 ((offsetX + xx - 1) / this.transform.localScale.x, -(offsetY + yy - 3) / this.transform.localScale.y, 0f);
		}
	}

	private void drawField (int px, int py, Color col, int height){
		int yy = (boxSizeY+1)*height*py-1;
		int xx = (boxSizeX+1)*px+ Mathf.FloorToInt(px/16);

		for (int y = yy; y< (yy+(boxSizeY*height)); y++){	
			for (int x = xx; x< (xx+boxSizeX) ; x++) {
				boxes.SetPixel(offsetX+x, boxes.height - ( offsetY+y), col);
	     	}
     	}
		boxes.Apply();	
	}

	private void drawInit(int height){
		boxes = new Texture2D((int)renderer.bounds.size.x, (int)renderer.bounds.size.y, TextureFormat.RGBA32, false);
		lastStates = new bool[sizeX, sizeY / height];
		for (int x = 0; x<sizeX; x++) {
			for (int y = 0; y<sizeY/height; y++) {
					lastStates[x,y] = false;
					drawField (x, y, offColor, height);
				}
			}
		}	

}
