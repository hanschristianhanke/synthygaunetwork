﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class ColorPresets {

	//public enum AvailableColors {Background = 0, Button_Off = 1, Button_On = 2, Step_Off = 3, Step_On = 4, Piano = 5};

	/*public static Color[] colors = 	new Color[]{     	new Color (0.08235f, 0.07450f, 0.14117f), new Color (0.45098f, 0.47450f, 0.49803f), 
														new Color (0.89019f, 0.89803f, 0.83137f), new Color (0.07450f, 0.68627f, 0.56078f), 
														new Color (0.89411f, 0.40392f, 0.40784f), new Color (0.03921f, 0.341176f, 0.278431f)};
*/
	public static string theme_name = "default";
	// StyleA = 0, StyleB = 1, StyleC = 2, PianoWhite = 3, PianoBlack = 4, Option=5, MenuSub = 6
	public static Color [] button_off = new Color[] {AvailableColors._Button_Off, AvailableColors._Step_Off, AvailableColors._Step_Off, AvailableColors._Piano, AvailableColors._Button_Off, AvailableColors._Step_Off, AvailableColors._Step_On};
	public static Color [] button_on = new Color[] {AvailableColors._Button_On, AvailableColors._Step_On, AvailableColors._Step_On, AvailableColors._Button_On, AvailableColors._Step_On, AvailableColors._Step_On, AvailableColors._Button_On};
	public static Color button_font_on = AvailableColors._Text;
	public static Color button_font_off = AvailableColors._Text;

	// Default, Piano ( Layer 1,2), Menu, MenuSub
	public static Color[][] button_adv_off = new Color[][] {new Color[] {AvailableColors._Text, AvailableColors._Button_On}, new Color[] {AvailableColors._Step_Off, AvailableColors._Button_Off}, new Color[] {AvailableColors._Button_Off, AvailableColors._Button_Off}, new Color[] {AvailableColors._Text, AvailableColors._Button_Off}};
	public static Color[][] button_adv_on =  new Color[][] {new Color[] {AvailableColors._Text, AvailableColors._Button_On}, new Color[] {AvailableColors._Step_On, AvailableColors._Button_Off}, new Color[] {AvailableColors._Piano, AvailableColors._Piano}, new Color[] {AvailableColors._Text, AvailableColors._Button_Off}};
	
	public static Color[] label = new Color[] {AvailableColors._Button_Off, AvailableColors._Button_Off};
	public static Color[] label_text = new Color[] {AvailableColors._Step_Off, AvailableColors._Text};
	
	public static Color slider_background = AvailableColors._Button_Off;
	public static Color slider_knob = AvailableColors._Text;
	// 0 = pan, 1 = volume
	public static Color[][] slider_color = 	new Color[][] {	new Color[] {AvailableColors._Button_Off, AvailableColors._Button_On, AvailableColors._Button_Off},
											new Color[] {AvailableColors._Button_On, AvailableColors._Step_On}};
	public static string [] slider_label = new string[] {"Pan", "Volume"};

	// White, WhiteMarked, Black, BlackMarked
	public static Color[] harmony = new Color[] {AvailableColors._Piano, AvailableColors._Button_On, AvailableColors._Button_Off, AvailableColors._Button_On};

	// Background, notSet, set, frame
	public static Color[] MatrixOverview =  new Color[] {AvailableColors._Button_Off, AvailableColors._Step_Off, AvailableColors._Step_On, AvailableColors._Button_On};


	public static void updateColors (Color pBackground, Color pButton_Off, Color pButton_On, Color pStep_Off, Color pStep_On, Color pPiano, Color pText){
		AvailableColors.setColors(pBackground, pButton_Off, pButton_On, pStep_Off, pStep_On, pPiano, pText);

		theme_name = "default";
		// StyleA = 0, StyleB = 1, StyleC = 2, PianoWhite = 3, PianoBlack = 4, Option=5, MenuSub = 6
		button_off = new Color[] {AvailableColors._Button_Off, AvailableColors._Step_Off, AvailableColors._Step_Off, AvailableColors._Piano, AvailableColors._Button_Off, AvailableColors._Step_Off, AvailableColors._Step_On};
		button_on = new Color[] {AvailableColors._Button_On, AvailableColors._Step_On, AvailableColors._Step_On, AvailableColors._Button_On, AvailableColors._Step_On, AvailableColors._Step_On, AvailableColors._Button_On};
		button_font_on = AvailableColors._Text;
		button_font_off = AvailableColors._Text;
		
		// Default, Piano ( Layer 1,2), Menu, MenuSub
		button_adv_off = new Color[][] {new Color[] {AvailableColors._Text, AvailableColors._Button_On}, new Color[] {AvailableColors._Step_Off, AvailableColors._Button_Off}, new Color[] {AvailableColors._Button_Off, AvailableColors._Button_Off}, new Color[] {AvailableColors._Text, AvailableColors._Button_Off}};
		button_adv_on =  new Color[][] {new Color[] {AvailableColors._Text, AvailableColors._Button_On}, new Color[] {AvailableColors._Step_On, AvailableColors._Button_Off}, new Color[] {AvailableColors._Piano, AvailableColors._Piano}, new Color[] {AvailableColors._Text, AvailableColors._Button_Off}};
		
		label = new Color[] {AvailableColors._Button_Off, AvailableColors._Button_Off};
		label_text = new Color[] {AvailableColors._Step_Off, AvailableColors._Text};
		
		slider_background = AvailableColors._Button_Off;
		slider_knob = AvailableColors._Text;
		// 0 = pan, 1 = volume
		slider_color = 	new Color[][] {	new Color[] {AvailableColors._Button_Off, AvailableColors._Button_On, AvailableColors._Button_Off},
										new Color[] {AvailableColors._Button_On, AvailableColors._Step_On}};
		slider_label = new string[] {"Pan", "Volume"};
		
		// White, WhiteMarked, Black, BlackMarked
		harmony = new Color[] {AvailableColors._Piano, AvailableColors._Button_On, AvailableColors._Button_Off, AvailableColors._Button_On};
		
		// Background, notSet, set, frame
		MatrixOverview =  new Color[] {AvailableColors._Button_Off, AvailableColors._Step_Off, AvailableColors._Step_On, AvailableColors._Button_On};

		ViewComponent [] views = GameObject.FindObjectsOfType<ViewComponent> ();
		foreach (ViewComponent cmp in views) {
			cmp.redraw();
		}
		GameObject.Find ("Main Camera").camera.backgroundColor = AvailableColors._Background;
	}
}
