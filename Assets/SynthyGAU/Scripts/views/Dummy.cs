using System;
using System.Collections;
using System.Collections.Generic;

public static class Dummy
{
	public static bool [,] states = new bool[64,72];
	public static int selectedrange = 0; // (0-3)
	public static int viewRowBegin = 0;


	public static bool useDrag = false;
	public static bool dragValue = false;	
	
	public static Hashtable instuments = new Hashtable();
	//public static Hashtable<HashSet<string>> voices = new HashSet<HashSet<string>>();
	
	/*public static string [][] voices = new string [][] {new string [] {"Abel", "voice 1", "voice 2", "voice 3", "voice 4", "voice 5", "voice 6"},
													new string [] {"Bebel", "voice 1", "voice 2", "voice 3", "voice 4", "voice 5", "voice 6"},
													new string [] {"Cebel", "voice 1", "voice 2", "voice 3", "voice 4", "voice 5", "voice 6"},
													new string [] {"Debel", "voice 1", "voice 2", "voice 3", "voice 4", "voice 5", "voice 6"}
													}; 
	*/
	public static string [] selectedCategory = new string[8];
	public static string [] selectedVoice = new string[8];
	
	public static int selectedTrack = 0;

	public static int selectedHarmony = -1;
	public static int [][] selectedHarmonyNotes = new int [][] { 	new int[]{1,4,6,10}, new int[]{1,4,7,11},
																	new int[]{1,4,7,11}, new int[]{1,4,8,12},
																	new int[]{1,5,6,11}, new int[]{1,5,8,11},
																	new int[]{1,5,7,12}, new int[]{1,5,9,1}};
}



