﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ViewComponent : MonoBehaviour {
	protected List<GameObject> layers = new List<GameObject>();	
		

	private float posX = 0;
	private float targetPosX = 0;
	private float currentVelocityX = 0;

	private float posY = 0;
	private float targetPosY = 0;
	private float currentVelocityY = 0;

	private float duration = 0;
	private float delay = 0;
	private float timer = 0;

	private bool transposing = false;

	private SynthyGAUScreen finishObject;

	private Vector3 pos;

	public Vector2 onStagePosition;
	public Vector2 offStagePosition;
	public bool visibleByDefault = true;

	public virtual void redraw(){
	}
	
	protected void newLayers(int num){
		for (int i = 0; i<num; i++){
			/*GameObject thisLayer = (GameObject)Instantiate (Resources.Load ("Theme/PixelPlane"));
			thisLayer.transform.parent = this.transform;
			thisLayer.transform.rotation = new Quaternion(0, 0,0,0);
			thisLayer.transform.localScale = new Vector3(1,1,0);
			thisLayer.transform.localPosition = new Vector3(0,0,-0.1f);
			thisLayer.renderer.material = new Material(Shader.Find("GUI/Text Shader"));
			thisLayer.name ="Layer"+layers.Count;*/
			layers.Add(gimmeGimmeGimme());	
		}
	}

	protected GameObject gimmeGimmeGimme(){
		GameObject thisLayer = (GameObject)Instantiate (Resources.Load ("Theme/PixelPlane"));
		thisLayer.transform.parent = this.transform;
		thisLayer.transform.rotation = new Quaternion(0, 0,0,0);
		thisLayer.transform.localScale = new Vector3(1,1,0);
		thisLayer.transform.localPosition = new Vector3(0,0,-0.1f);
		thisLayer.renderer.material = new Material(Shader.Find("GUI/Text Shader"));
		thisLayer.name ="Layer"+layers.Count;
		return thisLayer;
	}

	protected Texture2D calculateColor (Texture2D texture, Color [] foregroundColor, Rect offset = new Rect()){
		Color newColor;
		Rect toUse = offset.width != 0?offset:new Rect(0,0,texture.width,texture.height);
		Texture2D newTexture = new Texture2D(texture.width, texture.height, TextureFormat.RGBA32, false);
			
		for (int unit = 0; unit < foregroundColor.Length-1; unit++){
			int startX = (int)(unit * (toUse.width / (foregroundColor.Length-1)) + toUse.xMin);
			int endX = (int)((unit+1) * (toUse.width / (foregroundColor.Length-1)) + toUse.xMin);
			for (int x = startX; x< endX; x++){
				float lerp = (float)(x-startX)/(endX-startX);
				newColor = (foregroundColor[unit] * (1-lerp)) + (foregroundColor[(unit+1)] * lerp);					
				for (int y = 0; y<texture.height; y++){
					newTexture.SetPixel(x,y, new Color(newColor.r, newColor.g, newColor.b, texture.GetPixel(x,y).a));
				}
			}
		}
		newTexture.Apply();
		return 
			newTexture;
	}

	public void move (SynthyGAUScreen.AnimationType type, float pdelay, float pduration, SynthyGAUScreen done){
		duration = pduration;
		delay = pdelay;

		pos= transform.position;

		if (type == SynthyGAUScreen.AnimationType.MoveIn) {
			Debug.Log ("pos "+name+" move in");
			pos.x = offStagePosition.x;
			pos.y = offStagePosition.y;

			targetPosX = onStagePosition.x;
			targetPosY = onStagePosition.y;
		} else {
			Debug.Log ("pos "+name+" move out");
			pos.x = onStagePosition.x;
			pos.y = onStagePosition.y;
			
			targetPosX = offStagePosition.x;
			targetPosY = offStagePosition.y;
		}
		posX = pos.x;
		posY = pos.y;
		transform.position = pos;
		finishObject = done;
		transposing = true; 
	}

	protected void moveUpdate(){
		if (transposing) {		
			timer += Time.deltaTime;
			
			if (timer > delay){
				if (Mathf.Abs (targetPosY - posY) <= 0.5f && Mathf.Abs (targetPosX - posX) <= 0.5f) { 			
					transposing = false;
					timer = 0;
					finishObject.moveFinished (name);
				} else {
					Vector3 position = transform.position;
					
					//posX = Mathf.SmoothDamp (posX, targetPosX, ref currentVelocityX, duration);
					posX = Mathf.Lerp (pos.x, targetPosX, (timer-delay)/duration);
					position.x = posX;
					//	transform.position = position;
					
					//posY = Mathf.SmoothDamp (posY, targetPosY, ref currentVelocityY, duration);
					posY = Mathf.Lerp (pos.y, targetPosY, (timer-delay)/duration);
					position.y = posY;
					transform.position = position;
				}
			}						
		}
	}

	void Update(){
		moveUpdate ();
	}
}
