﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SelectContainerView : ViewComponent {
	
	static ScrollContainerView categoryContainer;
	static ScrollContainerView voiceContainer;
	
	// Use this for initialization
	void Awake () {
        init();
	}

    private bool initialized = false;
    public void init()
    {
        if (initialized) return;
      //  Destroy(gameObject.transform.FindChild("Plane").gameObject);
        categoryContainer = gameObject.transform.FindChild("ScrollContainer_Category").GetComponent<ScrollContainerView>();
        voiceContainer = gameObject.transform.FindChild("ScrollContainer_Voice").GetComponent<ScrollContainerView>();
        categoryContainer.init();
        voiceContainer.init();
        initialized = true;
    }


    public void fillValues(string[] cathegories, Instrument[] instruments, string selectedCathegorie, Instrument selectedInstrument)
    {
        fillCathegorieValues(cathegories,selectedCathegorie);
        fillInstrumentValues(instruments, selectedInstrument);
	}

    public void fillCathegorieValues(string[] cathegories, string selectedCathegorie)
    {
        categoryContainer.fillValues((object[])cathegories);
        categoryContainer.setButtonActive(selectedCathegorie);
    }

    public void fillInstrumentValues(Instrument[] instruments, Instrument selectedInstrument)
    {
        voiceContainer.fillValues((object[])instruments);
        voiceContainer.setButtonActive(selectedInstrument);
    }
}
