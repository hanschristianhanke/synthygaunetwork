using UnityEngine;
public static class AvailableColors
{
	public static Color Button_Off = new Color (0.08235f, 0.07450f, 0.14117f); 
	public static Color Step_Off = new Color (0.45098f, 0.47450f, 0.49803f);
	public static Color Piano = new Color (0.89019f, 0.89803f, 0.83137f);
	public static Color Button_On = new Color (0.07450f, 0.68627f, 0.56078f); 
	public static Color Step_On = new Color (0.89411f, 0.40392f, 0.40784f); 
	public static Color Background = new Color (0.29803f, 0.29803f, 0.32156f); 
	public static Color Text = new Color (0.89019f, 0.89803f, 0.83137f);

	public static Color _Button_Off = new Color (0.08235f, 0.07450f, 0.14117f); 
	public static Color _Step_Off = new Color (0.45098f, 0.47450f, 0.49803f);
	public static Color _Piano = new Color (0.89019f, 0.89803f, 0.83137f);
	public static Color _Button_On = new Color (0.07450f, 0.68627f, 0.56078f); 
	public static Color _Step_On = new Color (0.89411f, 0.40392f, 0.40784f); 
	public static Color _Background = new Color (0.29803f, 0.29803f, 0.32156f); 
	public static Color _Text = new Color (0.89019f, 0.89803f, 0.83137f);


	public static void setColors (Color pBackground, Color pButton_Off, Color pButton_On, Color pStep_Off, Color pStep_On, Color pPiano, Color pText){
		_Background = pBackground;
		_Button_Off = pButton_Off;
		_Button_On = pButton_On;
		_Step_Off = pStep_Off;
		_Step_On = pStep_On;
		_Piano = pPiano;
		_Text = pText;
	}
}

