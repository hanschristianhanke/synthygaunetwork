﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ButtonView : ViewComponent, ITextChangeableComponent
{
	public enum ButtonTypes {text, pause, play, stop, plus, minus, editselector, follow, pianooctave, pianomove, option, setup, sampler, synthy, sequenzer, arrange, effect, pianoinput};
	public enum ButtonStyles {StyleA = 0, StyleB = 1, StyleC = 2, PianoWhite = 3, PianoBlack = 4, MenuStyle = 5, MenuSub = 6};
	public enum ButtonAdvStyles {Default = 0, Piano = 1, Menu = 2, MenuSub = 3};
	public enum ButtonBehaviors {Background = 0, Image = 1, BackgroundAndImage = 2};
	
	public ButtonTypes buttonType = ButtonTypes.text;
	public ButtonStyles buttonStyle = ButtonStyles.StyleA;
	public ButtonAdvStyles buttonAdvStyle = ButtonAdvStyles.Default;

	public ButtonBehaviors setStateBehave = ButtonBehaviors.Image;
	public ButtonBehaviors setStateAdvancedBehave = ButtonBehaviors.Background;

	public string caption;
	public string additionalData;
	public bool useBackgroundColor = false;
	public Color overrideColor;
	public bool useOverrideColor = false;

	private GameObject layer1;
	private GameObject layer2;
	
	private Texture2D button;
	private TextMesh text;
		
	private List<Texture2D> offTextures = new List<Texture2D>();
	private List<Texture2D> onTextures = new List<Texture2D>();
	private ButtonController buttonController;
    protected bool activeState;
	protected bool activeStateAdv;

	private bool isFading = false;
	private float targetValue = 0;
	private float actValue = 0;
	private float deltaValue = 0;
	private float timeToFade = 0;

	private float flashTime = 0.5f;
	private bool isFlashing = false;
	private float flashAmount = 1f;
	private Color orgColor;

	private float t = 0;

	protected bool initialized = false;

	private GameObject flashLayer;
	private Color flashColor;

	public void Awake () {
		buttonController = gameObject.GetComponent<ButtonController>();
		
		button = new Texture2D(1,1, TextureFormat.RGBA32, false);
		button.SetPixel(1,1, Color.white);
		//renderer.material = new Material(Shader.Find("Default-Diffuse"));
		//renderer.material.shader = Shader.Find("Transparent/Cutout/Soft Edge Unlit");
		renderer.material.shader = Shader.Find("GUI/Text Shader");
		renderer.material.color = ColorPresets.button_off[(int)buttonStyle];
		Vector3 localScale = transform.localScale;
		localScale.z = 1;

		flashLayer = gimmeGimmeGimme ();
		flashLayer.name = "flashLayer";
		flashLayer.SetActive (false);

		transform.localScale = localScale;
		button.Apply();	

	//	renderer.material.mainTexture = button;
		
		if (buttonType != ButtonTypes.text){
			//newLayers(System.IO.File.Exists("Theme/icons_icon_"+buttonType.ToString()+"_02_on")?2:1);
			newLayers (2);
		} else {
			text = gameObject.GetComponentInChildren<TextMesh>();
			float newScaleX = 1f/transform.localScale.x;
			float newScaleY = 1f/transform.localScale.y;
			if (text){
				text.gameObject.transform.localScale = new Vector3(newScaleX/newScaleX,newScaleY/newScaleX,0);
				text.characterSize = 0.01f;
			}
            setText(caption);
		}
		loadTextures ();
		redraw();
		//setActiveAdvanced (false);
        //setActive(activeState);

		setState (setStateBehave, false);
		setState (setStateAdvancedBehave, false);
		initialized = true;
		//setActive (true);
	}

	void Update (){
		if (isFading) {
			Debug.Log (actValue + " -- " + targetValue);
			actValue = Mathf.SmoothDamp (actValue, targetValue, ref deltaValue, timeToFade);
			renderer.material.color = actValue * ColorPresets.button_on[(int)buttonStyle] +  (1-actValue) * ColorPresets.button_off[(int)buttonStyle];
			isFading = (actValue != targetValue);
		}

		if (isFlashing) {
			/*
			renderer.material.color = orgColor * (1- (flashAmount/flashTime)) + Color.white *  (flashAmount/flashTime);
			flashAmount -= Time.deltaTime;
			if (flashAmount < 0){
				isFlashing = false;
				renderer.material.color = orgColor;
			}
			*/
			flashAmount -= Time.deltaTime;
			flashColor.a = (flashAmount/flashTime);
			flashLayer.renderer.material.color = flashColor;
			if (flashAmount < 0){
				isFlashing = false;
				flashLayer.SetActive (false);
			}
		}
		base.moveUpdate ();
		/*
		t += Time.deltaTime;
		if (t>3) {
			t=0;
			flash (0.3f);
				}*/
	}

	public virtual void setText(string newText){
        if (text){
            text.text = newText;
		}
    }

	private void loadTextures(){
		if (buttonType != ButtonTypes.text){
			offTextures = new List<Texture2D>();
			onTextures = new List<Texture2D>();
			for (int i = 0; i<layers.Count; i++){		
				offTextures.Add((Texture2D)Resources.Load ("Theme/icons_icon_"+buttonType.ToString()+"_0"+(i+1)+"_off"));
				onTextures.Add((Texture2D)Resources.Load ("Theme/icons_icon_"+buttonType.ToString()+"_0"+(i+1)+"_on"));
			}
		} 
	}
	
	override public void redraw(){
		/*renderer.material.color = ColorPresets.button_off[(int)buttonStyle];
		//renderer.material.shader = Shader.Find ("Transparent/Cutout/Soft Edge Unlit");
		for (int i = 0; i<layers.Count; i++){		
			layers[i].renderer.material.color =  (activeState)?(Color) ColorPresets.button_adv_on[(int)buttonAdvStyle][i]:(Color) ColorPresets.button_adv_off[(int)buttonAdvStyle][i];
		}*/
	/*	activeState = !activeState;
		activeStateAdv = !activeStateAdv;
		setActive (!activeState);
		setActiveAdvanced (!activeStateAdv);*/
		setState (setStateBehave, activeState);
	}	

	public virtual void setColor( Color newColor){
		overrideColor = newColor;
		renderer.material.color = newColor;
	}

	public virtual void setActive(bool state){
		if (activeState != state) {
			activeState = state;
			targetValue = actValue = state ? 1 : 0;
			setState (setStateBehave, state);
			isFading = false;
	 	}
	}

	public virtual void setActive(bool state, float duration){
		if (activeState != state) {
			activeState = state;
			timeToFade = duration;
			targetValue = state ? 1 : 0;
			isFading = true;
		}
	}

	public virtual void setActiveAdvanced(bool state){
		if (activeStateAdv != state){
			activeStateAdv = state;
			setState (setStateAdvancedBehave, state);
		}
	}

	public virtual void flash (){
		//orgColor = renderer.material.color;
		/*if (setStateBehave == ButtonBehaviors.Background) {
			orgColor = activeState ? ColorPresets.button_on[(int)buttonStyle] : ColorPresets.button_off[(int)buttonStyle];
		} else  if (setStateAdvancedBehave == ButtonBehaviors.Background) {
			orgColor = (activeStateAdv) ? ColorPresets.button_on [(int)buttonStyle] : ColorPresets.button_off [(int)buttonStyle];
		}*/
		flashLayer.SetActive (true);

		flashTime = 0.5f;
		flashAmount = 0.5f;
		isFlashing = true;
		flashColor = Color.white;
	}

	public virtual void flash (float duration){
		flashLayer.SetActive (true);
		//orgColor = activeState ? ColorPresets.button_on [(int)buttonStyle] : ColorPresets.button_off [(int)buttonStyle];
		flashTime = duration;
		flashAmount = duration;
		isFlashing = true;
		flashColor = Color.white;
	}

	private void setState (ButtonBehaviors behave, bool state){
	//	isFlashing = false;
		if (!useOverrideColor) {
			if (buttonType == ButtonTypes.text) {
				renderer.material.color = (state) ? ColorPresets.button_on [(int)buttonStyle] : ColorPresets.button_off [(int)buttonStyle];
				if (text) {
						text.color = (state) ? ColorPresets.button_font_on : ColorPresets.button_font_off;
				}
			} else {
				if (behave == ButtonBehaviors.Image || behave == ButtonBehaviors.BackgroundAndImage) {
					for (int i=0; i<layers.Count; i++) {		
						layers [i].renderer.material.mainTexture = (state) ? onTextures [i] : offTextures [i];	
						layers [i].renderer.material.color = (state) ? (Color)ColorPresets.button_adv_on [(int)buttonAdvStyle] [i] : (Color)ColorPresets.button_adv_off [(int)buttonAdvStyle] [i];
					}
				} 

				if (behave == ButtonBehaviors.Background || behave == ButtonBehaviors.BackgroundAndImage) {
					renderer.material.color = (state) ? ColorPresets.button_on [(int)buttonStyle] : ColorPresets.button_off [(int)buttonStyle];
				}
			}
		} else {

			renderer.material.color = overrideColor;
		}
	}
}
