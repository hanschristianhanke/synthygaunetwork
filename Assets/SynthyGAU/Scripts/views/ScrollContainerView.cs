﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



public class ScrollContainerView : ViewComponent {
	public enum EntryTypes {Button_Category, Button_Voice, Button_LoadSave};
	public EntryTypes entryType = EntryTypes.Button_Category;
	
	public int rows = 11;
	public int columns = 1;
	public string defaultCaption = "";

	private float entrySizeY;

    private object[] values= new object[0];
	private List <GameObject> _rowEntries = new List<GameObject>();
    public List<GameObject> rowEntries
    {
        get { return this._rowEntries; }
    }

	void Awake () {
        init();
	}

    private bool initialized = false;
    public void init()
    {
        if (initialized) return;
        initialized = true;

        //Debug.Log("intit");

        int r = 0;
        int actY = 0;
        for (int iRow = 0; iRow < rows; iRow++)
        {
            for (int iColumn = 0; iColumn < columns; iColumn++)
            {
                GameObject entry = (GameObject)Instantiate(Resources.Load("Components/" + entryType.ToString()));
                entry.transform.parent = gameObject.transform;
                entry.transform.rotation = new Quaternion(0, 0, 0, 0);

                if (iRow % 2 == 1 && entryType == EntryTypes.Button_Voice)
                {
                    entry.transform.localScale = entry.transform.localScale - new Vector3(0, 1, 0);
                }
                entry.transform.localPosition = new Vector3(iColumn * (entry.renderer.bounds.size.x + 1), -actY, 0);
                entry.name = "Category_Entry_" + entryType.ToString() + "_" + r;
				entry.GetComponent<ButtonView>().caption = defaultCaption;
                _rowEntries.Add(entry);
                r++;

                actY = (iColumn == (columns - 1)) ? actY + (int)entry.transform.localScale.y + 1 : actY;
            }
        }
    }

    public void fillValues(object[] values)
    {
        clearEntries();
        this.values = values;
        for (int i = 0; i < values.Length; i++)
        {
            ButtonView buttonView = _rowEntries[i].GetComponent<ButtonView>();
            buttonView.caption = values[i].ToString();
            buttonView.setText(values[i].ToString());
        }
        redraw();
        
	}

    private void clearEntries(){
        foreach (GameObject entrie in _rowEntries)
        {
            ButtonView buttonView = entrie.GetComponent<ButtonView>();
            buttonView.setText("");
            buttonView.setActive(false);
        }
    }

    public object setButtonActive(object captionObject)
    {
        object returnObject = null;
        for (int i = 0; i < values.Length; i++ )
        {
            object obj = values[i];
            bool selected = (obj.Equals(captionObject));
            _rowEntries[i].GetComponent<ButtonView>().setActive(selected);
            if (selected) returnObject = obj;
        }

        return returnObject;
    }

	public object setButtonActive(int x, int y){
        object returnObject = null;
        int maxlen = values.Length;
        for (int iRow=0; iRow < rows; iRow++){
			for (int iColumn=0;	iColumn < columns; iColumn++){
                bool selected = iRow == y && iColumn == x;
                int index = iRow * columns + iColumn;
                if (index < maxlen) {                 
                    _rowEntries[index].GetComponent<ButtonView>().setActive(selected);
                    if (selected)
                    {
                        returnObject = values[index];
                    }
                }

			}
		}
        return returnObject;
	}
}
