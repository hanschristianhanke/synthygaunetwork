﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MatrixOverviewViewALT : ViewComponent {
	private int boxSizeX = 5;
	private int boxSizeY = 5;
	private int sizeX = 64;
	private int sizeY = 72;

	private int scaleX;
	private int scaleY;

	//private Texture2D boxes;

	private int counter = 0;

    private int _offsetX = 0;
    public int offsetX
    {
        get { return this._offsetX; }
    }
	private int _offsetY = 0;
    public int offsetY
    {
        get { return this._offsetY; }
    }

	private Texture2D selectionTexture;

	private TextMesh debugText;

	private static bool[,] lastStates;

	
	private bool pan = false;
	private static Texture2D background72;
	private static Texture2D background24;

	private static GameObject activeBox;
	private static GameObject[,] activeBoxes;
	private static int STEPS = 64;
	private static int NOTES = 72;

	int harmonyLength;
	int height;

	// Use this for initialization
	void Awake () {
		background72 = ((Texture2D)Resources.Load ("Theme/icons_matrix_overview_72"));
		background24 = ((Texture2D)Resources.Load ("Theme/icons_matrix_overview_24"));
		activeBox = gimmeGimmeGimme ();

		activeBox.renderer.material.color = ColorPresets.MatrixOverview [2];
		renderer.material.shader = Shader.Find("GUI/Text Shader");
		renderer.material.mainTexture = background72;
		renderer.material.color = ColorPresets.MatrixOverview [1];

		activeBoxes = new GameObject[STEPS,NOTES];
		float width = 1f / 64;
		float height = 1f / 72;

		float boxWidth = width- 1f/transform.localScale.x;
		float boxHeight = height- 1f/transform.localScale.y;

		for (int actX=0; actX<STEPS; actX++){
			for (int actY=0; actY<NOTES; actY++){	
				GameObject tempBox = (GameObject)Instantiate(activeBox);
				tempBox.transform.parent = this.transform;
				tempBox.name = "nr "+actX+"x"+actY;
				//tempBox.transform.localPosition = new Vector3((actX * tempBox.transform.localScale.x)+1, (actY * tempBox.transform.localScale.y)+1, -0.1f); 

				//tempBox.transform.localPosition = new Vector3(width,height, -0.1f);
				tempBox.transform.localScale = new Vector3( boxWidth, boxHeight, 0) ;
				tempBox.transform.localPosition = new Vector3(width * actX, -height*actY,-0.1f);
				tempBox.SetActive(false);
				activeBoxes[actX, actY] = tempBox;
			}
		}

		activeBox.SetActive (false);
		scaleX = sizeX * (boxSizeX + 1) + ((sizeX / 16) - 2); //(int)gameObject.transform.localScale.x;
		scaleY = sizeY * (boxSizeY + 1); //(int)gameObject.transform.localScale.y;
		//boxes = new Texture2D((int)renderer.bounds.size.x, (int)renderer.bounds.size.y, TextureFormat.RGBA32, false);

		//renderer.material.mainTexture = boxes;

		//boxes.filterMode = FilterMode.Point;
		//renderer.material.color = ColorPresets.MatrixOverview[0];

//		Debug.Log (scaleX + " /// " + scaleY);
		/*for (int y = 0; y< boxes.height; y++){	
			for (int x = 0; x< boxes.width ; x++) {
				boxes.SetPixel(x,y, ColorPresets.MatrixOverview[0]);
			}
		}*/

		//_offsetX = Mathf.FloorToInt ((boxes.width - scaleX) / 2);
		//_offsetY = Mathf.FloorToInt ((boxes.height - scaleY) / 2);

		//base.newLayers (1);
		//base.layers[0].renderer.material.shader = Shader.Find("Unlit/Transparent");	

		//debugText = GameObject.Find ("DebugText").GetComponent<TextMesh> ();


		// KANN RAUS!!!!
		redraw(new bool[64,72]);
	}

	// CONTROLLER

	// x=step (0-63), y=note (0-71)
	public void redraw (bool [,] states){
		Debug.Log ("alle 1");
		lastStates = states;

		if (states != null) {
			/*renderer.material.shader = Shader.Find("GUI/Text Shader");
			renderer.material.mainTexture = states.GetLength(1) == 72?background72:background24;
			renderer.material.color = ColorPresets.MatrixOverview [1];
			*/
			int lengthY = states.GetLength(1);

			Debug.Log("Size ******* "+states.GetLength(0)+" // "+states.GetLength(1));

			for (int actX=0; actX<STEPS; actX++){
				for (int actY=0; actY<NOTES; actY++){	
					activeBoxes[actX, actY].SetActive(actY < lengthY &&  states[actX, actY]);
				}
			}

			/*base.layers [0].renderer.material.mainTexture = selectionTexture;
			base.layers [0].gameObject.transform.parent = this.transform;
			base.layers [0].gameObject.transform.localScale = new Vector3 (((boxSizeX + 1) * 16) / this.transform.localScale.x, ((boxSizeY + 1) * 8 * height) / this.transform.localScale.y, 0f);

			/*
			Color onColor = ColorPresets.MatrixOverview [2];

			//_offsetX = Mathf.FloorToInt ((boxes.width - scaleX) / 2);
			//_offsetY = Mathf.FloorToInt ((boxes.height - scaleY) / 2) + (Dummy.selectedHarmony != -1 ? 2 : 0);

			harmonyLength = (Dummy.selectedHarmony != -1) ? Dummy.selectedHarmonyNotes [Dummy.selectedHarmony].Length : 12;
			height = 12 / harmonyLength;
			/*
			for (int x = 0; x<sizeX; x++) {
					for (int y = 0; y<sizeY/height; y++) {
							drawField (x, y, (states [x, y] ? onColor : offColor), height);
					}
			}	

			//harmonyLength = (Dummy.selectedHarmony != -1) ? Dummy.selectedHarmonyNotes [Dummy.selectedHarmony].Length : 12;
			//heigth = 12 / harmonyLength;

			selectionTexture = new Texture2D (1, 1, TextureFormat.RGBA32, false);

			Color frameColor = ColorPresets.MatrixOverview [3];
			frameColor.a = 0.3f;

			selectionTexture.SetPixel (1, 1, frameColor);
			selectionTexture.Apply ();

			setViewFrame (0, 0);*/
		}
	}

	// x=step (0-63), y=note (0-71)
	public void redraw (int step, int note, bool [,] states){
		lastStates = states;
		/*
		Color onColor = ColorPresets.MatrixOverview [2];
		Color offColor = ColorPresets.MatrixOverview [1];

		if (Dummy.selectedHarmony == -1) {
			drawField (step, note, (states [step, note]?onColor:offColor), 1 );
		} else {
			int octaveNote = note%12;
			int harmonyLength = (Dummy.selectedHarmony != -1) ? Dummy.selectedHarmonyNotes [Dummy.selectedHarmony].Length : 12;
			int heigth = 12 / harmonyLength;

			for (int i=0; i<harmonyLength; i++){
				if (octaveNote == Dummy.selectedHarmonyNotes [Dummy.selectedHarmony][i]){
					drawField (step, i, (states [step, note]?onColor:offColor), heigth );
					break;
				}
			}
		}*/
		activeBoxes [step, note].SetActive (states[step,note]);
		/*for (int actX=0; actX<STEPS; actX++){
			for (int actY=0; actY<NOTES; actY++){	
				activeBoxes[boxCounter].SetActive(actY < lengthY &&  states[actX, actY]);
				boxCounter++;
			}
		}*/
	}

	public override void redraw(){
		redraw (lastStates);
	}

	


	// VIEW

	public void setViewFrame (int step, int note){
		int sizeYFrame;
		int beginYFrame;

		sizeYFrame = 8;

		beginYFrame = Mathf.Clamp(note, 0, (sizeY/height)-sizeYFrame);
		step = Mathf.Clamp (step, 0, 48);
		drawFrame (beginYFrame, sizeYFrame, step);
	}

	private void drawFrame (int beginYFrame, int sizeYFrame, int step){
		int yy = (boxSizeY+1)*height*beginYFrame;
		int xx = (boxSizeX+1)*step+ Mathf.FloorToInt(step/16);

		base.layers [0].gameObject.transform.localPosition = new Vector3 ( (_offsetX+xx-1)/this.transform.localScale.x, -(_offsetY+yy-3) /this.transform.localScale.y , -0.1f);
	}

	private void drawField (int px, int py, Color col, int height){
		int yy = (boxSizeY+1)*height*py-1;
		int xx = (boxSizeX+1)*px+ Mathf.FloorToInt(px/16);

		for (int y = yy; y< (yy+(boxSizeY*height)); y++){	
			for (int x = xx; x< (xx+boxSizeX) ; x++) {
			//	boxes.SetPixel(_offsetX+x, boxes.height - ( _offsetY+y), col);
	     	}
     	}
		//boxes.Apply();	
	}
}
