﻿// TODO Multitouch fehlt!!!

using UnityEngine;
using System.Collections;

using TouchScript.Events;
using TouchScript.Gestures;

//[ExecuteInEditMode]
public class StepMatrixModulView : ViewComponent {
	StepButtonView [,] stepButtons = new StepButtonView[16,8];

	//TextMesh debugText;
	Rect mySize;
	bool[] tapValue = new bool[8];

	bool dummyInit = true;
	GameObject dummyOverview;


	public StepMatrixModulView(){
		Debug.Log ("so");
		}

	// Use this for initialization
	void Awake () {
		Destroy (gameObject.transform.FindChild("Plane").gameObject);
		for (int x=0; x<16; x++){
			for (int y=0; y<8; y++){
				GameObject step = (GameObject)Instantiate(Resources.Load ("Components/StepButton"));	
				step.transform.parent = gameObject.transform;
				step.transform.localPosition = new Vector3 (x*97, -y*97,0);
				step.transform.rotation = new Quaternion(0,0,0,0);
				step.name = "StepButtonView_"+x+"_"+y;
				stepButtons[x,y] = step.GetComponent<StepButtonView>();
				step.GetComponent<StepButtonView>().setActive(true);
			}
		}

		//initDummy ();
		/*Vector2 olP = Camera.main.WorldToScreenPoint (gameObject.transform.position);
		Vector2 urP = Camera.main.WorldToScreenPoint (new Vector3((16*97), (8*97),0));
		mySize = new Rect (olP.x, olP.y, urP.x, urP.y);*/
		//GetComponent<PanGesture>().StateChanged += OnPanStateChanged;
		//GetComponent<PressGesture>().StateChanged += OnPressStateChanged;	
		setAll(Dummy.states);	
	}

	// step (0-15)
	public void setFlash (int step, float duration){
		for (int y = 0; y < 8; y++){
			stepButtons[step, y].flash(duration);
		}
	}

	// track (0-7), step (0-15)
	public void setNote(int track, int step, bool state){
		stepButtons[track, step].setActive(state);
	}

	// states [16,8]
	public void setAll (bool [,] states){
		for (int x = 0; x<16; x++) {
			for (int y = 0; y<8; y++) {
				stepButtons[x,y].setActive(states[x,y] );
			}
		}
	}

	// In den Controller
	// Bitte gehen Sie weiter. Hier gibt es nichts zu sehen...

	/*private void dummyMessageStepChanged(int x, int y){
				if (Dummy.selectedHarmony == -1 || true) {
						int xx = x + Dummy.selectedrange*16;
						int yy = y + Dummy.viewRowBegin;
						Dummy.states [xx, yy] = !Dummy.states [xx, yy];
						if (xx >= Dummy.selectedrange*16 && xx < Dummy.selectedrange*16 + 16) {
							setNote(x,y, Dummy.states [xx, yy]);
						}
					if (dummyInit) {
						setAll (Dummy.states);
						dummyOverview = GameObject.Find ("MatrixOverview");
						if (dummyOverview != null){dummyOverview.GetComponent<MatrixOverviewView> ().redraw (Dummy.states);}
						dummyInit = false;
					} else {
				
						if (dummyOverview != null){dummyOverview.GetComponent<MatrixOverviewView> ().redraw (xx,yy, Dummy.states);}
					}
				}
	}

	private void dummyMessageTouchBegin (int touchNr, int x, int y){
			int xx = x+ Dummy.selectedrange*16;
			int yy = y+Dummy.viewRowBegin;
			tapValue[touchNr] = Dummy.states [xx, yy];
	}
		
	void initDummy(){
		for (int x=0; x < 64; x++){
			for (int y=0; y<72; y++){
				Dummy.states[x,y] = (Random.value<0.5f);
			}
		}
	}*/

}
