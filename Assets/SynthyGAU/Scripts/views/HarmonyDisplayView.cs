﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class HarmonyDisplayView : ViewComponent {

	public int harmonyNr = 0;

	private Texture2D button;
	private Texture2D [] texturesOn = new Texture2D[4];
	private Texture2D [] texturesOff = new Texture2D[4];
	private bool state;

	private Transform thisTransform;

	private Texture2D genericWhiteKeysSpecial;
	private Texture2D genericBlackKeysSpecial;

	private Texture2D genericWhiteKeys;
	private Texture2D genericBlackKeys;

	// Use this for initialization
	void Awake () {
		thisTransform = transform;
		button = new Texture2D(1,1, TextureFormat.RGBA32, false);
		button.SetPixel(1,1, Color.white);
		button.Apply ();
		//renderer.material.shader = Shader.Find("Transparent/Cutout/Soft Edge Unlit");
		//renderer.material.mainTexture = button;
		newLayers(4);
	//	layers [2].transform.localScale = new Vector3(1,0.6f,0);
	//	layers [3].transform.localScale = new Vector3(1,0.6f,0);
	    loadTexture ();
		redraw ();

		prepareTexture (Dummy.selectedHarmonyNotes[harmonyNr]);
		setActive (false);
	}

	private void loadTexture(){
		/*for (int i = 0; i<layers.Count; i++) {	
			texturesOff [i] = (Texture2D)Resources.Load ("Theme/Harmonies/icons_icon_harmony_" + harmonyNr+"_0" + (i + 1));
			texturesOn [i] = (Texture2D)Resources.Load ("Theme/Harmonies/icons_icon_harmony_" + harmonyNr+"_0" + (i + 1));
		}*/
		genericWhiteKeysSpecial = (Texture2D)Resources.Load ("Theme/icons_icon_harmony_blank_01_Special");
		genericBlackKeysSpecial = (Texture2D)Resources.Load ("Theme/icons_icon_harmony_blank_03_Special");

		genericWhiteKeys = (Texture2D)Resources.Load ("Theme/icons_icon_harmony_blank_01_106");
		genericBlackKeys = (Texture2D)Resources.Load ("Theme/icons_icon_harmony_blank_03_106");
	}

	private void prepareTexture(int [] harmony){
		for (int i=0; i<layers.Count; i++){

			if (!System.IO.File.Exists (Application.persistentDataPath + "/harmony_0" + harmonyNr + "_0"+(i+1)+".png")) {
				texturesOff [i] = prepareKeys (harmony, (i<2)? (harmonyNr%4 == 0? genericWhiteKeysSpecial: genericWhiteKeys): (harmonyNr%4 == 0? genericBlackKeysSpecial: genericBlackKeys), i%2 == 1);
				SaveTextureToFile (texturesOff [i], "/harmony_0" + harmonyNr + "_0"+(i+1)+".png");
			} else {
				Texture2D newTex = new Texture2D((harmonyNr%4==0)?genericWhiteKeysSpecial.width:genericWhiteKeys.width , (harmonyNr%4==0)?genericWhiteKeysSpecial.height:genericWhiteKeys.height, TextureFormat.RGBA32 ,false);
				newTex.LoadImage(File.ReadAllBytes (Application.persistentDataPath +"/harmony_0"+harmonyNr+"_0"+(i+1)+".png"));
				newTex.filterMode = FilterMode.Point;
				newTex.anisoLevel = 0;
				texturesOff [i] = newTex;
			}
			texturesOn [i] = texturesOff [i];
		}
	}

	private Texture2D prepareKeys(int [] harmony, Texture2D tex, bool off){
				float lastAlpha = 0;
				int counter = 0;

				int ia = 0;
				bool deleteKey = false;
				bool pause = false;

				Texture2D newTex = new Texture2D (tex.width, tex.height, TextureFormat.RGBA32 ,false);

				for (int x = 0; x < tex.width; x++) {
			        for (int y = 0; y < tex.height; y++) {
						Color32 pxl = tex.GetPixel (x, y);
						pxl = containsValue(harmony, pxl.r/ 20) == off && pxl.a == 255? Color.white:new Color (0,2,0,0);
				        newTex.SetPixel (x, y, pxl);
					}
				}
				newTex.Apply ();
				newTex.filterMode = FilterMode.Point;
				return newTex;
		}

	private bool containsValue (int [] array, int value){
		foreach (int i in array) {
			if (value == i){
				return true;
			}
		}
		return false;
	}

	override public void redraw(){
		for (int i = 0; i<layers.Count; i++) {		
			layers[i].transform.localPosition = (i<2)?new Vector3(0,0,0): new Vector3 (0,0,-0.1f);
			layers[i].renderer.material.color = (Color)ColorPresets.harmony [i];
			//texturesOff [i] = calculateColor ((Texture2D)Resources.Load ("Theme/Harmonies/icons_icon_harmony_" + harmonyNr+"_0" + (i + 1)), (i<2)?(Color)ColorPresets.harmony [0]:(Color)ColorPresets.harmony [2]);
			//texturesOn [i] = calculateColor ((Texture2D)Resources.Load ("Theme/Harmonies/icons_icon_harmony_" + harmonyNr+"_0" + (i + 1)), (Color)ColorPresets.harmony [i]);
		}
	}

	public void setActive (bool state){
		this.state = state;
		for (int i = 0; i<layers.Count; i++) {		
			layers[i].renderer.material.mainTexture = state?texturesOn[i]:texturesOff[i];
		}
	}

	private void SaveTextureToFile( Texture2D texture, string fileName)
	{
		byte [] data = texture.EncodeToPNG ();
		File.WriteAllBytes (Application.persistentDataPath + fileName, data);
	}
}
