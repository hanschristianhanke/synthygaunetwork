﻿using System;

public interface ITextChangeableComponent
{
     void setText(string newText);
}
