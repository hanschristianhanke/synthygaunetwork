﻿using UnityEngine;
using System.Collections;

public class Note {

	public float pitch;
	public float endBuffer;

	private int channels;
	private float bufferCounter = 0f;

	public Note (int endBuffer, float pitch, int channels){
		/*Note temp = new Note ();
		temp.buffersLeft = startBuffer;
		temp.pitch = pitch;
		return temp; */
		this.channels = channels;
		this.pitch = pitch;
		this.endBuffer = endBuffer;
	}

	public int nextSampleIndex (){
		bufferCounter += pitch;
		//bufferCounter += 2;
		return (int)bufferCounter;
	}

	private bool _done = false;
	public bool Done
	{
		get { return _done; }
	}
}
