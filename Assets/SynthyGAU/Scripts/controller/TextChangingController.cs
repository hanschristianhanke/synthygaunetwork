﻿using UnityEngine;
using System.Collections;
using System;

public class TextChangingController : AController
{
    private ButtonView textChangeableComponent;

	void Start () {
        textChangeableComponent = GetComponent<ButtonView>();
	}

    public override void OnNotify(NotificationData notificationData) {
        
        if (!gameObject.activeInHierarchy) return;
        if (notificationData.notificationType != notificationType) return;

        Instrument selectedInstrument = applicationDataModel.audioDataModel.getTrackInstrument(Array.IndexOf(UIComponentConstants.SoundSelector, name));
        if (selectedInstrument == null) return;

        textChangeableComponent.setText(selectedInstrument.name);
    }
}
