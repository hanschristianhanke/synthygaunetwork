﻿using UnityEngine;
using System.Collections;
using System;

public class ActivatableButtonController : ButtonController {
	
	public override void OnNotify(NotificationData notificationData){
        if (!gameObject.activeInHierarchy) return;
		if (notificationData.notificationType != notificationType) return;

		buttonView.setActive(applicationDataModel.getBooleanValue(name));
        base.OnNotify(notificationData);
	}
}