﻿using UnityEngine;
using System.Collections;
using System;

[RequireComponent (typeof (ButtonView))]
public class SliderController : AController {
	
	protected SliderView sliderView;
    private String[] sliderNames = UIComponentConstants.Slider;
    private String[] colorSliderNames = UIComponentConstants.Color_Slider;
	
	protected void Awake () {
		sliderView = gameObject.GetComponent<SliderView>();
	}
	
	public override void OnNotify(NotificationData notificationData){
       
		if (!gameObject.activeInHierarchy) return;
        if ((notificationData.notificationType == NotificationType.PANSLIDERDRAGGED) || (notificationData.notificationType == NotificationType.VOLUMESLIDERDRAGGED) || (notificationData.notificationType == NotificationType.SELECTTRACKVOLUMEEDITMODE))
        {
            if (name.Equals(sliderNames[0]))
            {
                updateVolumeSlider();
            }
            else if (name.Equals(sliderNames[1]))
            {
                updatePanSlider();
            }
            else if (name.Equals(sliderNames[2]))
            {
                sliderView.setValue(audioDataModel.pianoVolume);
            }
        }
        else if (notificationData.notificationType == NotificationType.SELECTTHEMECOLOR || notificationData.notificationType == NotificationType.SELECTTHEMECOLORTYPE)
        {
            if (notificationData.notifingGameObject == null) return;
            string componentId = notificationData.notifingGameObject.name;
            //if (!componentId.Equals(name)) return;

            //Debug.Log("slide " + colorDataModel.selectedthemecolor  + " " + name + " " + notificationData.notifingGameObject.name);

            Color color = colorDataModel.getThemeColor(colorDataModel.selectedthemecolor);

            if (name.Equals(colorSliderNames[0]))
            {
                sliderView.setValue(color.r);
            }
            else if (name.Equals(colorSliderNames[1]))
            {
                sliderView.setValue(color.g);
            }
            else if (name.Equals(colorSliderNames[2]))
            {
                sliderView.setValue(color.b);
            }
        }
	}

    private void updateVolumeSlider()
    {
        int trackId = applicationDataModel.audioDataModel.trackvolumediteselection;
        sliderView.setValue((trackId == -1) ? audioDataModel.mainVolume : audioDataModel.getTackVolume(trackId));
    }

    private void updatePanSlider()
    {
        int trackId = applicationDataModel.audioDataModel.trackvolumediteselection;
        sliderView.setValue((trackId == -1) ? audioDataModel.mainPan : audioDataModel.getPanValue(trackId));
    }

    void OnMouseDrag()
    {
        Vector2 lp = calcMatrixPosition(Input.mousePosition);
        float val = Mathf.Clamp(lp.x / renderer.bounds.size.x, 0, 1);
        NotifyRootcontroller(new SliderDataChangeCommand(notificationType, gameObject.name, applicationDataModel, val));
    }

    private Vector2 calcMatrixPosition(Vector2 tp)
    {
        Vector2 lp = Camera.main.ScreenToWorldPoint(tp);
        lp.x = Mathf.FloorToInt(lp.x - transform.position.x);
        return lp;
    }
}
