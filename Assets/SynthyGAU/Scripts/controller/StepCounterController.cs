﻿using UnityEngine;
using System.Collections;

public class StepCounterController : AController {
	public override void OnNotify(NotificationData notificationData){
		if (notificationData.notificationType==NotificationType.RANGEBUTTONCLICKED) {
			NotifyRootcontroller(new StepMatrixDataChangeCommand(NotificationType.STEPBUTTONCKLICKED, "Button_Step_1_16", applicationDataModel));
		}else if (notificationData.notificationType==NotificationType.STEP){
            if (!applicationDataModel.follow) return;

			int current = audioDataModel.selectedrange;
			int shuldbe = audioDataModel.step/16;

			if (current==shuldbe) return;
			string id ="Button_Step_1_16";
			if (shuldbe==1){
				id ="Button_Step_17_32";
			} if (shuldbe==2){
				id ="Button_Step_33_48";	
			} if (shuldbe==3){
				id ="Button_Step_49_64";	
			}

			NotifyRootcontroller(new StepMatrixDataChangeCommand(NotificationType.STEPBUTTONCKLICKED, id, applicationDataModel));
            rootController.OnNotify(new NotificationData(NotificationType.MATRIXDATACHENGED, null, null));
		}
	}
}
