﻿using UnityEngine;
using System.Collections;

public class DisplayStepController : AController {
	
	protected DisplayStepView displayStepView;
	
	protected void Start () {}

	private void init(){
		displayStepView = gameObject.GetComponent<DisplayStepView>();
	}
	
	public override void OnNotify(NotificationData notificationData){
		if (notificationData.notificationType != notificationType) return;
		if (displayStepView==null) {
			init();
		}

		displayStepView.setStep(applicationDataModel.audioDataModel.step);
	}
}
