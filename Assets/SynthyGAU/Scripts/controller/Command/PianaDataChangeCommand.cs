﻿using UnityEngine;
using System.Collections;
using System;

public class PianaDataChangeCommand : DataChangeCommand{

        private String[] sliderNames = UIComponentConstants.Slider;
		private float newValue;

        public PianaDataChangeCommand(AController.NotificationType notificationType, String componentID, ApplicationDataModel applicationDataModel) : base(notificationType, componentID, applicationDataModel)
		{}

        public override void execute()
        {
            switch (notificationType)
            {
                case AController.NotificationType.OCTAVESELECTION:
                {
                    AudioDataModel audioDatamodel = applicationDataModel.audioDataModel;
                    
                    String[] componentIDs = UIComponentConstants.Button_Octave;
                    setBooleanComponentValues(componentIDs, false);
                    int octaveId = Array.IndexOf(componentIDs, componentID);

                    if (!((octaveId == audioDatamodel.firstOctave) || (octaveId == audioDatamodel.secondOctave))) { 
                        if (audioDatamodel.fistOctaveWasSetLast) { audioDatamodel.secondOctave = octaveId; } else { audioDatamodel.firstOctave = octaveId; }
                        audioDatamodel.fistOctaveWasSetLast = !audioDatamodel.fistOctaveWasSetLast;
                    }
                    applicationDataModel.setBooleanValue(componentIDs[audioDatamodel.firstOctave], true);
                    applicationDataModel.setBooleanValue(componentIDs[audioDatamodel.secondOctave], true);
                }
                break;
                case AController.NotificationType.PIANOKEYCLICKED:
                {
                    //Debug.Log(componentID);
                    //Button_PianoWhite_0
                    //Button_PianoBlack_0
                }
                break;
            }
        }
}
