﻿using UnityEngine;
using System.Collections;
using System;

public class ScreenChangingCommand : DataChangeCommand
{
    AController.NotificationType originNotificationType;

    public ScreenChangingCommand(AController.NotificationType notificationType, String componentID, ApplicationDataModel applicationDataModel, AController.NotificationType originNotificationType) : base(notificationType, componentID, applicationDataModel) { this.originNotificationType = originNotificationType; }

    public override void execute()
    {
        if (notificationType != AController.NotificationType.CHANGESCREEN) return;

        switch (originNotificationType)
        {
            case AController.NotificationType.SELECTINSTRUMENTEDITMODE:{
                if (applicationDataModel.audioDataModel.track == -1) applicationDataModel.currentScreen = ScreenManagerView.AvailableScreens.sequencer; else applicationDataModel.currentScreen = ScreenManagerView.AvailableScreens.edit;     
            }
            break;
            case AController.NotificationType.MENUBUTTONCLICKED:{
                switch (applicationDataModel.selectedmenubutton)
                {
                    case 2:
                        applicationDataModel.currentScreen = ScreenManagerView.AvailableScreens.setup;
                    break;
                    case 3:
                        applicationDataModel.currentScreen = ScreenManagerView.AvailableScreens.sequencer;
                    break;
					case 5:
						applicationDataModel.currentScreen = ScreenManagerView.AvailableScreens.effect;
					break;
                    default:
                        applicationDataModel.currentScreen = ScreenManagerView.AvailableScreens.sequencer;
                    break;

                }
            }
            break;
            case AController.NotificationType.SETUPTYPEBUTTONCLICKED:
            {
                switch (applicationDataModel.selectedmenubutton)
                {
                    case 0:
                        applicationDataModel.currentScreen = ScreenManagerView.AvailableScreens.setup;
                        break;
                    case 1:
                        applicationDataModel.currentScreen = ScreenManagerView.AvailableScreens.setup;
                        break;
                    case 2:
                        applicationDataModel.currentScreen = ScreenManagerView.AvailableScreens.setupTheme;
                        break;

                }
            }
            break;
        }

       
    }
}
