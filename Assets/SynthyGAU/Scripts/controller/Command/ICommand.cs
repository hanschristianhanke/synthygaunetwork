using System;
using UnityEngine;

public interface ICommand
{
	void execute();
	String getNotificationType();
	String getComponentID();
}


