﻿using UnityEngine;
using System;


public class StepMatrixDataChangeCommand : DataChangeCommand{

        private Vector2 lp;
		public StepMatrixDataChangeCommand(AController.NotificationType notificationType, String componentID, ApplicationDataModel applicationDataModel): base(notificationType, componentID, applicationDataModel){}
        public StepMatrixDataChangeCommand(AController.NotificationType notificationType, String componentID, ApplicationDataModel applicationDataModel, Vector2 lp) : base(notificationType, componentID, applicationDataModel) {
            this.lp = lp;
        }

	private int lastY = 0;
	
		public override void execute(){
            switch (notificationType)
            {
                case AController.NotificationType.STEPBUTTONCKLICKED:
                    {
                        String[] componentIDs = UIComponentConstants.Button_Step;
                        setBooleanComponentValues(componentIDs, false);
                        applicationDataModel.setBooleanValue(componentID, true);
                        applicationDataModel.audioDataModel.selectedrange = Array.IndexOf(componentIDs, componentID);
                    }
                    break;
                case AController.NotificationType.FOLLOWSTEPBUTTONCLICKED:
                    {
                        bool val = applicationDataModel.getBooleanValue(componentID);
                        applicationDataModel.setBooleanValue(componentID, !val);
                        applicationDataModel.follow = !val;
                    }
                    break;
                case AController.NotificationType.SELECTINSTRUMENTEDITMODE:
                    {
                        bool val = !applicationDataModel.getBooleanValue(componentID);

                        String[] componentIDs = UIComponentConstants.Button_EditMode;
                        setBooleanComponentValues(componentIDs, false);
                        applicationDataModel.setBooleanValue(componentID, val);
                        applicationDataModel.audioDataModel.track = (val) ? Array.IndexOf(componentIDs, componentID) : -1;

                    }
                    break;
                case AController.NotificationType.MATRIXDATACHENGED:
                    {
                        // maybe unnessesary
                    }
                    break;
                case AController.NotificationType.MATRIXBUTTONDATACHANGED:
                    {
                        AudioDataModel audioDataModel = applicationDataModel.audioDataModel;
                        int x = (int)lp.x;
                        int y = (int)lp.y;
                        bool val = audioDataModel.getCurrentToneActive(x, y);
                        audioDataModel.setCurrentToneActive(x, y, !val);
						Debug.Log ("!STEP "+x+" / "+y+" / "+val);
                    }
                    break;
                case AController.NotificationType.SELECTHARMONY:
                    {
                        bool val = !applicationDataModel.getBooleanValue(componentID);

                        String[] componentIDs = UIComponentConstants.Button_Harmony;
                        setBooleanComponentValues(componentIDs, false);

                        applicationDataModel.setBooleanValue(componentID, val);
                        applicationDataModel.audioDataModel.selectedharmony = (val) ? Array.IndexOf(componentIDs, componentID) : -1;
                    }
                    break;
                case AController.NotificationType.WIPEMATRIXOVERVIEWVERTICALLY:
                    {
                        AudioDataModel audioDataModel = applicationDataModel.audioDataModel;
                      
			if (lastY != lp.x){
				Debug.Log("wipe up " + lp.x+"  "+Time.fixedTime);
						if (lp.x > 0)
                        {
                            audioDataModel.getGetDataFromCurrentTrack().transposeUp();
                        }
						else if (lp.x < 0)
                        {
                            audioDataModel.getGetDataFromCurrentTrack().transposeDown();
                        }
                    }
			lastY = Mathf.RoundToInt(lp.x);
			}
                    break;
                case AController.NotificationType.WIPEMATRIXOVERVIEWHORIZONTALLY:
                    {
                        AudioDataModel audioDataModel = applicationDataModel.audioDataModel;
                       
                        if (lp.x > 0)
                        {
                            audioDataModel.getGetDataFromCurrentTrack().transposeStepForward();
                        }
						else if (lp.x < 0)
                        {
                            audioDataModel.getGetDataFromCurrentTrack().transposeStepBackward();
                        }
                    }
                    break;
                case AController.NotificationType.SELECTMATRIXOVERVIEWPART:
                    {
                        AudioDataModel audioDataModel = applicationDataModel.audioDataModel;
                        int x = (int)lp.x / 16;
                        int y = (int)lp.y / 8;

                        audioDataModel.selectedhight = y;
                        if (x > audioDataModel.range) return;
                        audioDataModel.selectedrange = x;

                        String[] componentIDs = UIComponentConstants.Button_Step;
                        setBooleanComponentValues(componentIDs, false);
                        applicationDataModel.setBooleanValue(componentIDs[x], true);

                    }
                    break;
            }
		}
}
