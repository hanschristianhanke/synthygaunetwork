﻿using UnityEngine;
using System.Collections;
using System;

public class SliderDataChangeCommand : DataChangeCommand{

        private String[] sliderNames = UIComponentConstants.Slider;
		private float newValue;
	
		public SliderDataChangeCommand(AController.NotificationType notificationType, String componentID, ApplicationDataModel applicationDataModel, float newValue): base(notificationType, componentID, applicationDataModel)
		{
			this.newValue = newValue;
		}
			
		public override void execute(){
			
			switch(notificationType){
			case AController.NotificationType.VOLUMESLIDERDRAGGED:{
                
                AudioDataModel audioDataModel = applicationDataModel.audioDataModel;
                int trackId = applicationDataModel.audioDataModel.trackvolumediteselection;
                
                if (componentID.Equals(sliderNames[0]))
                {
                    if (trackId == -1) { audioDataModel.mainVolume = newValue; } else { audioDataModel.setTackVolume(trackId, newValue); }
                }
                else
                {
                    audioDataModel.pianoVolume = newValue;
                }
			}	
			break;
            case AController.NotificationType.PANSLIDERDRAGGED:
            {
                AudioDataModel audioDataModel = applicationDataModel.audioDataModel;
                int trackId = applicationDataModel.audioDataModel.trackvolumediteselection;
                if (trackId == -1) { audioDataModel.mainPan = newValue; } else { audioDataModel.setPanValue(trackId, newValue); }
            }
            break;
            case AController.NotificationType.SELECTTHEMECOLOR:
            {
                String[] componentIDs = UIComponentConstants.Color_Slider;
                int colorPartId = Array.IndexOf(componentIDs, componentID);
                
                ColorDataModel colorDataModel = applicationDataModel.colorDataModel;
                Color themeColor = colorDataModel.getThemeColor(colorDataModel.selectedthemecolor);

                switch (colorPartId)
                {
                    case 0:
                        themeColor.r = newValue;
                    break;
                    case 1:
                        themeColor.g = newValue;
                    break;
                    case 2:
                        themeColor.b = newValue;
                    break;
                }
                colorDataModel.setThemeColor(colorDataModel.selectedthemecolor,themeColor);
                colorDataModel.setColorValue(UIComponentConstants.Button_Color[colorDataModel.selectedthemecolor], themeColor);
            }
            break;
            }
            
		}
}
