﻿using UnityEngine;
using System.Collections;
using System;

public class MenuDataChangeCommand :  DataChangeCommand
{
   

    public MenuDataChangeCommand(AController.NotificationType notificationType, String componentID, ApplicationDataModel applicationDataModel) : base(notificationType, componentID, applicationDataModel) { }

    public override void execute()
    {
        switch (notificationType)
        {
            case AController.NotificationType.SHOWMENU:
                {
                    bool value = !applicationDataModel.getBooleanValue(componentID);
                    applicationDataModel.setBooleanValue(componentID, value);
                    applicationDataModel.showMenu = value;
                }
                break;
            case AController.NotificationType.MENUBUTTONCLICKED:
                {
                    String[] componentIDs = UIComponentConstants.Button_Screen;
                    int menuId = Array.IndexOf(componentIDs, componentID);
                    //bool value = !applicationDataModel.getBooleanValue(componentID);
                    setBooleanComponentValues(componentIDs, false);
                    applicationDataModel.setBooleanValue(componentID, true);
                    applicationDataModel.selectedmenubutton = menuId;
                }
                break;
            case AController.NotificationType.SETUPTYPEBUTTONCLICKED:
                {
                    String[] componentIDs = UIComponentConstants.Button_Setup;
                    int menuId = Array.IndexOf(componentIDs, componentID);
                    setBooleanComponentValues(componentIDs, false);
                    applicationDataModel.setBooleanValue(componentID, true);
                    applicationDataModel.selectedsetupbutton = menuId;
                }
                break;
            case AController.NotificationType.SELECTTHEMECOLORTYPE:
                {

                    String[] componentIDs = UIComponentConstants.Button_Color;
                    int id = Array.IndexOf(componentIDs, componentID);
                    applicationDataModel.colorDataModel.selectedthemecolor = id;
                }
                break;
            case AController.NotificationType.RESETCOLOR:
                {
                    Debug.Log("RESET");

                    ColorDataModel colorDataModel = applicationDataModel.colorDataModel;

                    Color color = AvailableColors.Background;
                    Color newColor = new Color(color.r, color.g, color.b);
                    colorDataModel.setColorValue(UIComponentConstants.Button_Color[0], newColor);
                    colorDataModel.setThemeColor(0, newColor);

                    color = AvailableColors.Button_Off;
                    newColor = new Color(color.r, color.g, color.b);
                    colorDataModel.setColorValue(UIComponentConstants.Button_Color[1], newColor);
                    colorDataModel.setThemeColor(1, newColor);

                    color = AvailableColors.Button_On;
                    newColor = new Color(color.r, color.g, color.b);
                    colorDataModel.setColorValue(UIComponentConstants.Button_Color[2], newColor);
                    colorDataModel.setThemeColor(2, newColor);

                    color = AvailableColors.Step_Off;
                    newColor = new Color(color.r, color.g, color.b);
                    colorDataModel.setColorValue(UIComponentConstants.Button_Color[3], newColor);
                    colorDataModel.setThemeColor(3, newColor);

                    color = AvailableColors.Step_On;
                    newColor = new Color(color.r, color.g, color.b);
                    colorDataModel.setColorValue(UIComponentConstants.Button_Color[4], newColor);
                    colorDataModel.setThemeColor(4, newColor);

                    color = AvailableColors.Piano;
                    newColor = new Color(color.r, color.g, color.b);
                    colorDataModel.setColorValue(UIComponentConstants.Button_Color[5], newColor);
                    colorDataModel.setThemeColor(5, newColor);

                    color = AvailableColors.Text;
                    newColor = new Color(color.r, color.g, color.b);
                    colorDataModel.setColorValue(UIComponentConstants.Button_Color[6], newColor);
                    colorDataModel.setThemeColor(6, newColor);

                    ColorPresets.updateColors(colorDataModel.getThemeColor(0), colorDataModel.getThemeColor(1), colorDataModel.getThemeColor(2), colorDataModel.getThemeColor(3), colorDataModel.getThemeColor(4), colorDataModel.getThemeColor(5), colorDataModel.getThemeColor(6));
                    //ColorPresets.updateColors(AvailableColors.Background, AvailableColors.Button_Off, AvailableColors., colorDataModel.getThemeColor(3), colorDataModel.getThemeColor(4), colorDataModel.getThemeColor(5), colorDataModel.getThemeColor(6));
                }
                break;
            case AController.NotificationType.SETNEWCOLOR:
                {
                    ColorDataModel colorDataModel = applicationDataModel.colorDataModel;
                    ColorPresets.updateColors(colorDataModel.getThemeColor(0), colorDataModel.getThemeColor(1), colorDataModel.getThemeColor(2), colorDataModel.getThemeColor(3), colorDataModel.getThemeColor(4), colorDataModel.getThemeColor(5), colorDataModel.getThemeColor(6));
                }
                break; 
        }

    }
}
