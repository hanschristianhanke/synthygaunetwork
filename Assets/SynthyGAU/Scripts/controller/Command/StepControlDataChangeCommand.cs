using System;
using UnityEngine;

public class StepControlDataChangeCommand : DataChangeCommand
{

    public StepControlDataChangeCommand(AController.NotificationType notificationType, String componentID, ApplicationDataModel applicationDataModel) : base(notificationType, componentID, applicationDataModel) { }

    public override void execute()
    {

        AudioDataModel audioDataModel = applicationDataModel.audioDataModel;

        switch (notificationType)
        {
            case AController.NotificationType.BANKBUTTONCLICKED: {
                String[] componentIDs = UIComponentConstants.Button_Bank;
                int bankId = Array.IndexOf(componentIDs, componentID);

                if (applicationDataModel.clearmode)
                {
                    audioDataModel.clearBank(bankId);
                }
                else if (applicationDataModel.copymode)
                {
                    audioDataModel.copyBank(audioDataModel.bank, bankId);
                }
                else
                {
                    setBooleanComponentValues(componentIDs, false);
                    applicationDataModel.setBooleanValue(componentID, true);
                    audioDataModel.bank = bankId;
                }
            }
            break;
            case AController.NotificationType.PATTERNBUTTONCLICKED: {
                String[] componentIDs = UIComponentConstants.Button_Pattern;
                int patternId = Array.IndexOf(componentIDs, componentID);

                if (applicationDataModel.clearmode)
                {
                    audioDataModel.clearPattern(audioDataModel.bank, patternId);
                }
                else if (applicationDataModel.copymode)
                {
                    audioDataModel.copyPattern(audioDataModel.bank, audioDataModel.pattern, patternId); 
                }
                else
                {
                    setBooleanComponentValues(componentIDs, false);
                    applicationDataModel.setBooleanValue(componentID, true);
                    audioDataModel.pattern = patternId;
                }
            }
            break;
            case AController.NotificationType.RANGEBUTTONCLICKED: { 
                String[] componentIDs = UIComponentConstants.Button_Range;
                int stepId = Array.IndexOf(componentIDs, componentID);
                applicationDataModel.audioDataModel.range = stepId;

                setBooleanComponentValues(componentIDs, false);
                applicationDataModel.setBooleanValue(componentID, true);

                String[] buttonstepcomponentIDs = UIComponentConstants.Button_Step;
                setBooleanComponentDisabledValues(buttonstepcomponentIDs, true);

                for (int i = 0; i < stepId + 1 ; i++)
                {
                    applicationDataModel.setBooleanValue(buttonstepcomponentIDs[i] + "_disabled", false);
                }
            }
            break;
            case AController.NotificationType.EDITMODEBUTTONCLICKED:
                {
                    String[] componentIDs = UIComponentConstants.Button_Edit;
                    bool value = !applicationDataModel.getBooleanValue(componentID);
                    applicationDataModel.setBooleanValue(componentID, value);

                    string btnCopyName = componentIDs[0];
                    string btnClearName = componentIDs[1];

                    if (componentID.Equals(btnClearName))
                    {
                        applicationDataModel.setBooleanValue(btnCopyName, false);
                        applicationDataModel.copymode = false;
                        applicationDataModel.clearmode = value;
                    }
                    else if (componentID.Equals(btnCopyName))
                    {
                        applicationDataModel.setBooleanValue(btnClearName, false);
                        applicationDataModel.copymode = value;
                        applicationDataModel.clearmode = false;
                    }
                }
                break;
            case AController.NotificationType.PLAYMODEBUTTONCLICKED:
                {
                    String[] componentIDs = UIComponentConstants.Button_Control;
                    int controlId = Array.IndexOf(componentIDs, componentID);

                    setBooleanComponentValues(componentIDs, false);
                    applicationDataModel.setBooleanValue(componentID, true);

                    //applicationDataModel.playmode = ApplicationDataModel.Playmode.;
                    switch (controlId)
                    {
                        case 0: applicationDataModel.playmode = ApplicationDataModel.Playmode.PLAY; break;
                        case 1: applicationDataModel.playmode = ApplicationDataModel.Playmode.PAUSE; break;
                        case 2: applicationDataModel.playmode = ApplicationDataModel.Playmode.STOP; break;
                    }
                }
                break;
            case AController.NotificationType.BPMCHANGEBUTTONCLICKED:
                {
                    if (componentID.Equals(UIComponentConstants.Button_Bpm_Control[0]))
                    {
                        applicationDataModel.audioDataModel.bpm += 5;
                    }
                    else
                    {
                        applicationDataModel.audioDataModel.bpm -= 5;
                    }
                }
                break;
        }
    }
}


