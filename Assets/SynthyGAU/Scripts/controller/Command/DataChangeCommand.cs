﻿using System;
using UnityEngine;

public abstract class DataChangeCommand : ICommand{
		
		protected AController.NotificationType notificationType;
		protected String componentID;
		protected ApplicationDataModel applicationDataModel;
		
	//private String notificationType;
	//private String componentID;
	//private , ApplicationDataModel applicationDataModel	

		public DataChangeCommand(AController.NotificationType notificationType, String componentID, ApplicationDataModel applicationDataModel){
			this.componentID = componentID;
			this.applicationDataModel = applicationDataModel;
			this.notificationType = notificationType;
		}

        protected void setBooleanComponentValues(String[] componentNames, bool value){
            foreach (String componentName in componentNames)
            {
                applicationDataModel.setBooleanValue(componentName, value);
            }
        }

        protected void setBooleanComponentDisabledValues(String[] componentNames, bool value)
        {
            foreach (String componentName in componentNames)
            {
                applicationDataModel.setBooleanValue(componentName+"_disabled", value);
            }
        }
		
		public abstract void execute();

		public String getNotificationType(){
			return notificationType.ToString ();
		}

		public String getComponentID(){
			return componentID;
		}
}
