﻿using UnityEngine;
using System;
using System.Collections;

public class SelectContainerController : AController {

    SelectContainerView selectContainerView;

    void Awake()
    {
        selectContainerView = GetComponent<SelectContainerView>();
        selectContainerView.init();
    }

	public override void OnNotify(NotificationData notificationData){
        if (!(notificationData.notificationType == NotificationType.OPENSELECTINSTRUMENTCONTAINERBUTTONCLICKED || notificationData.notificationType == NotificationType.SELECTINSTRUMENTCATHEGORY)) return;

        int instrumentselection = applicationDataModel.audioDataModel.instrumentselection;

        Instrument selectedInstrument = null;
        if (instrumentselection != -1)
        {
            selectedInstrument = applicationDataModel.audioDataModel.getTrackInstrument(instrumentselection);
        }

        //Debug.Log(instrumentselection);

        if (notificationData.notificationType == NotificationType.OPENSELECTINSTRUMENTCONTAINERBUTTONCLICKED)
        {
            String id = notificationData.notifingGameObject.name;
            bool show = (instrumentselection != -1);
            gameObject.SetActive(show);

            if (show)
            {
                RessourceDataModel ressourceDataModel = applicationDataModel.ressourceDataModel;
                String cathegorie = ressourceDataModel.getCategoryForInstrument(selectedInstrument);

				GetComponent<SelectContainerView>().fillValues(ressourceDataModel.getInstrumentCategoryies(), ressourceDataModel.getInstrumentsFromCategory(cathegorie), cathegorie, selectedInstrument);
            }
        }
        else if (notificationData.notificationType == NotificationType.SELECTINSTRUMENTCATHEGORY)
        {
            RessourceDataModel ressourceDataModel = applicationDataModel.ressourceDataModel;
            Instrument[] instruments = ressourceDataModel.getInstrumentsFromCategory((String)notificationData.data);
			GetComponent<SelectContainerView>().fillInstrumentValues(instruments, selectedInstrument);
        }
	}
}