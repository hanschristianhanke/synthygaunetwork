﻿using UnityEngine;
using System.Collections;
using System;

[RequireComponent (typeof (ButtonView))]
public class ColorBoxController : AController {

    protected ButtonView buttonView;
    void Awake()
    {
        buttonView = gameObject.GetComponent<ButtonView>();
    }

	public override void OnNotify(NotificationData notificationData){
        if (notificationType == NotificationType.SELECTTHEMECOLOR || notificationType == NotificationType.RESETCOLOR)
        {
            //Debug.Log(name + " " + notificationType);
			gameObject.GetComponent<ButtonView>().setColor(colorDataModel.getThemeColor(colorDataModel.selectedthemecolor));
            //buttonView.setColor(new Color(0.5f,0.5f,0.5f));
        }
	}
}
