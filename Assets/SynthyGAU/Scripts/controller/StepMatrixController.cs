﻿using UnityEngine;
using System.Collections;

using TouchScript.Events;
using TouchScript.Gestures;

public class StepMatrixController : AController {
	
	private StepMatrixModulView stepMatrixModulView;

	protected void Awake () {
		init();
	}

	private void init(){
		stepMatrixModulView = gameObject.GetComponent<StepMatrixModulView>();
        GetComponent<PanGesture>().StateChanged += OnPanStateChanged;
        GetComponent<PressGesture>().StateChanged += OnPressStateChanged;	
	}

	public override void OnNotify(NotificationData notificationData){
        if (!gameObject.activeInHierarchy) return;

        switch (notificationData.notificationType)
        {
            case NotificationType.STEP:
            {
                if (!gameObject.activeInHierarchy) return;
                int min = audioDataModel.selectedrange * 16;
                int max = min + 15;
                int step = applicationDataModel.audioDataModel.step;

                if ((step >= min) && (step <= max))
                {
                    stepMatrixModulView.setFlash(step % 16, 0.5f);
                }
            }
            break;
            case NotificationType.MATRIXBUTTONDATACHANGED:
            {

                Vector2 lp = (Vector2)notificationData.data;
                int x = (int)lp.x;
                int y = (int)lp.y;
                bool val = audioDataModel.getCurrentToneActive(x, y);
                stepMatrixModulView.setNote(x, y, val);

			Debug.Log ("!!!!!!!STEP "+x+" / "+y+" / "+val);
            }
            break;
            case NotificationType.MATRIXDATACHENGED:
            {
                stepMatrixModulView.setAll(audioDataModel.getCurrentToneActiveValues());
            }
            break;
        }
	}

    // OUT track (0-7), step (0-15)
    private void OnPressStateChanged(object sender, GestureStateChangeEventArgs e)
    {
        PressGesture target;
        switch (e.State)
        {
            case Gesture.GestureState.Recognized:
               // i = 0;
                target = sender as PressGesture;
                foreach (TouchScript.TouchPoint tp in target.ActiveTouches)
                {
					Vector2 lp = calcMatrixPosition(tp.Position);
                    NotifyRootcontroller(new StepMatrixDataChangeCommand(notificationType,name,applicationDataModel,lp),lp);
					
                    //i++;
                }
                break;
        }
    }

    // OUT track (0-7), step (0-15)
    private void OnPanStateChanged(object sender, GestureStateChangeEventArgs e)
    {
		string output = "";
		PanGesture target = sender as PanGesture;
		int touchNr = 0;
//		Debug.Log ("hier");
		switch (e.State)
		{
			case Gesture.GestureState.Began:

			foreach (TouchScript.TouchPoint tp in target.ActiveTouches){
				Vector2 lp = calcMatrixPosition(tp.Position);
				// NOTIFY Beginn der Touchphase "touchNr". Setzten des StepButtons. Alle weiteren Buttons werden auf den gleichen Wert gesetzt!
				// NotifyRootcontroller(new StepMatrixDataChangeCommand(notificationType,name,applicationDataModel,lp),lp, touchNr);

				touchNr++;
			}
			break;

			case Gesture.GestureState.Changed:				
				foreach (TouchScript.TouchPoint tp in target.ActiveTouches){
				Vector2 lp = calcMatrixPosition(tp.Position);
				// NOTIFY witerer TouchVerlauf
				// NotifyRootcontroller(new StepMatrixDataChangeCommand(notificationType,name,applicationDataModel,lp),lp, touchNr);

				touchNr++;
			}
			break;

			case Gesture.GestureState.Ended:
			break;
		}
    }

    private Vector2 calcMatrixPosition(Vector2 tp){
		Vector2 lp = Camera.main.ScreenToWorldPoint(tp);
		lp.x = Mathf.FloorToInt(Mathf.Abs(lp.x - transform.position.x) / (97 + 1));
		lp.y = Mathf.FloorToInt(Mathf.Abs(lp.y - transform.position.y) / (97 + 1));
		return lp;
    }
}
