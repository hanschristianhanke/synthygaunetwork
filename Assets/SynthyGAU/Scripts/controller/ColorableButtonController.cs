﻿using UnityEngine;
using System.Collections;
using System;

[RequireComponent (typeof (ButtonView))]
public class ColorableButtonController : ButtonController {

    protected ButtonView buttonView;
    private String[] sliderNames = UIComponentConstants.Slider;
	
	protected void Awake () {
        buttonView = GetComponent<ButtonView>();
	}
	
	public override void OnNotify(NotificationData notificationData){
        NotificationType notificationType = notificationData.notificationType;
        if (notificationType == NotificationType.SELECTTHEMECOLORTYPE)
        {
            base.OnNotify(notificationData);
        }
        else if (notificationType == NotificationType.SELECTTHEMECOLOR || notificationType == NotificationType.RESETCOLOR)
        {
			GetComponent<ButtonView>().setColor(colorDataModel.getColorValue(name));
        }
	}
}
