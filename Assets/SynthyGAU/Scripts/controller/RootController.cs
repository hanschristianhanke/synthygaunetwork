﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RootController : AController {
	
	private AController[] subControllers; 
	private ApplicationDataModel applicationDataModel;
    private Sequencer sequencer;
    private Synthesizer synthesizer;
	
	void Start () {	
		applicationDataModel = new ApplicationDataModel();

		initController();

		InvokeRepeating("initApplicationDataModel",0.1f,1);
	}

	private void initController(){
		List<AController> tmp = new List<AController>();

		GameObject[] gameObjects = (GameObject[]) Resources.FindObjectsOfTypeAll(typeof(GameObject));


		foreach(GameObject gameObject in gameObjects){
			AController[] controllers = gameObject.GetComponents<AController>();
			if (controllers!=null){
                foreach (AController controller in controllers)
                {
                    tmp.Add(controller);
                }
			}
		}

		GameObject ca = GameObject.Find ("Main Camera");
        sequencer = ca.AddComponent<Sequencer>();
        synthesizer = ca.AddComponent<Synthesizer>();
        sequencer.synthesizer = synthesizer;

		tmp.Add(sequencer);
        tmp.Add(synthesizer);

		tmp.Remove(this);
		subControllers  = tmp.ToArray();
		
		foreach (AController controller in subControllers) {
			controller.DoInitialization(this, applicationDataModel);
		}
	}
	
	//----------------------------------------
	// init datamodel 
	//----------------------------------------
	
	private void initApplicationDataModel(){
		new Initializer(this, applicationDataModel).Initialize();
		CancelInvoke();
	}
	
	public override void OnNotify(NotificationData notificationData){
		ICommand command = notificationData.command;
		if (command!=null) {
			command.execute();
		}
		foreach (AController controller in subControllers) {
			controller.OnNotify(notificationData);
		}
	}
}
