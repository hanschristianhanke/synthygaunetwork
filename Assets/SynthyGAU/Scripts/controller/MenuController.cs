﻿using UnityEngine;
using System.Collections;

public class MenuController : AController {

    private bool animate = false;
    private float pos = 0;
    private float targetPos = 0;
    private float currentVelocity = 0;

    private float defaultSpeed = 0.5f;
    private float speed = 0.5f;

    public override void OnNotify(NotificationData notificationData) {
        if (!gameObject.activeInHierarchy) return;
        if (notificationData.notificationType != NotificationType.SHOWMENU) return;

        //buttonContainer = GameObject.Find("ScreenButton_Modul").transform;
        targetPos = applicationDataModel.showMenu ? (-323) : 0;
        speed = defaultSpeed;
        animate = true;
    }

    void Update()
    {
        if (animate)
        {
            if (Mathf.Abs(targetPos - pos) > 0.5f)
            {
                pos = Mathf.SmoothDamp(pos, targetPos, ref currentVelocity, speed);
                Vector3 position = transform.localPosition;
                position.x = pos;
                transform.localPosition = position;
            }
            else
            {
                animate = false;
            }
        }
    }
}
