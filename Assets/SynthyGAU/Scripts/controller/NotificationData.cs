using UnityEngine;
using System;


public class NotificationData
{
	
	private AController.NotificationType _notificationType;
	public AController.NotificationType notificationType
	{
    	get { return this._notificationType; }
	}
	
	private GameObject _notifingGameObject;
	public GameObject notifingGameObject
	{
    	get { return this._notifingGameObject; }
	}
	
	private ICommand _command;
	public ICommand command
	{
    	get { return this._command; }
	}

    private object _data;
    public object data
    {
        get { return this._data; }
    }
	
	public NotificationData (AController.NotificationType notificationType, GameObject notifingGameObject, ICommand command)
	{
        init(notificationType, notifingGameObject, command, null);
	}

    public NotificationData(AController.NotificationType notificationType, GameObject notifingGameObject, ICommand command, object data)
    {
        init(notificationType,notifingGameObject,command,data);
    }

    private void init(AController.NotificationType notificationType, GameObject notifingGameObject, ICommand command, object data)
    {
        this._notificationType = notificationType;
        this._notifingGameObject = notifingGameObject;
        this._command = command;
        this._data = data;
    }
}

