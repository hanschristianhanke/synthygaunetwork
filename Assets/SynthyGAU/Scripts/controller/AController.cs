﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class AController : MonoBehaviour {
	
	public NotificationType notificationType; 
	protected RootController rootController;
	protected ApplicationDataModel applicationDataModel;
    protected AudioDataModel audioDataModel;
    protected RessourceDataModel ressourceDataModel;
    protected ColorDataModel colorDataModel;
	protected NetworkHandler networkHandler;




	void Start () {
		networkHandler = GameObject.Find ("Main Camera").GetComponent<NetworkHandler> ();
	}



	
	public void DoInitialization(RootController rootController, ApplicationDataModel applicationDataModel) {
    	this.rootController = rootController; 
		this.applicationDataModel = applicationDataModel;
        this.audioDataModel = applicationDataModel.audioDataModel;
        this.ressourceDataModel = applicationDataModel.ressourceDataModel;
        this.colorDataModel = applicationDataModel.colorDataModel;
	}
	
	public abstract void OnNotify(NotificationData notificationData);
	
	public void NotifyRootcontroller(ICommand command){
		if (rootController==null) return;
		rootController.OnNotify(new NotificationData(notificationType, gameObject, command));

	}

	public void NotifyRootcontroller(ICommand command, object data)
    {
        if (rootController == null) return;
        rootController.OnNotify(new NotificationData(notificationType, gameObject, command, data));
		Debug.Log (command.GetType());
		if (command.getNotificationType().Equals ("MATRIXBUTTONDATACHANGED")) {
			networkHandler.notifyOthers (command, data, gameObject.name.ToString ());
		}
    }

	public void NotifyRootcontrollerByNotification(ICommand command, object data, GameObject gobj, AController.NotificationType nt)
	{
		if (rootController == null) return;
		rootController.OnNotify(new NotificationData(  nt , gobj, command, data));
	}

	/*public void notifyOthers(string rpctype, string notType, string id, object data){
		//	NotifyRootcontroller (new Root, data);
		System.Type nodeType = System.Type.GetType(rpctype);
		
		if (rpctype.Equals ("StepMatrixDataChangeCommand")) {
			NotifyRootcontroller(new StepMatrixDataChangeCommand(notificationType,name,applicationDataModel,(Vector2)data),(Vector2)data);
		}
		//NotifyRootcontroller(new StepMatrixDataChangeCommand(notificationType,name,applicationDataModel,lp),lp);
		
		//		NotifyRootcontroller (System.Activator(nodeType, new {notType, id, applicationDataModel}), data);
	}*/
	
	public enum NotificationType {BANKBUTTONCLICKED, PATTERNBUTTONCLICKED, RANGEBUTTONCLICKED, 
		EDITMODEBUTTONCLICKED, PLAYMODEBUTTONCLICKED, BPMCHANGEBUTTONCLICKED, STEPBUTTONCKLICKED,
		PANSLIDERDRAGGED, VOLUMESLIDERDRAGGED, STEP, FOLLOWSTEPBUTTONCLICKED, OPENSELECTINSTRUMENTCONTAINERBUTTONCLICKED,
        SELECTINSTRUMENTEDITMODE, SELECTINSTRUMENTCATHEGORY, SELECTINSTRUMENT, MATRIXDATACHENGED, MATRIXBUTTONDATACHANGED,
        SELECTTRACKVOLUMEEDITMODE, OCTAVESELECTION, PIANOKEYCLICKED, SELECTHARMONY, SELECTMATRIXOVERVIEWPART,
        WIPEMATRIXOVERVIEWHORIZONTALLY, WIPEMATRIXOVERVIEWVERTICALLY, CHANGESCREEN, SHOWMENU, MENUBUTTONCLICKED, SETUPTYPEBUTTONCLICKED,
        SELECTTHEMECOLORTYPE, SELECTTHEMECOLOR, SETNEWCOLOR, RESETCOLOR
	};

    public class NotificationTypeWrapper{
        private NotificationType _notificationType;
        public NotificationType notificationType
        {
            get { return this._notificationType; }
        }
        public NotificationTypeWrapper(NotificationType notificationType)
        {
            _notificationType = notificationType;
        }
    }
	
}
