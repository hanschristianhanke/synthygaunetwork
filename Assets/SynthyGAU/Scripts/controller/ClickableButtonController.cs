﻿using UnityEngine;
using System.Collections;

public class ClickableButtonController : ButtonController {

	public override void OnNotify(NotificationData notificationData){
		if (notificationData.notificationType != notificationType) return;
		if(notificationData.notifingGameObject != gameObject) return;
		
		//buttonView.setActive(true);
		//InvokeRepeating("deactivateButton",0.2f,1);
        base.OnNotify(notificationData);
	}
	
	private void deactivateButton(){
		buttonView.setActive(false);
		CancelInvoke();
	}
}
