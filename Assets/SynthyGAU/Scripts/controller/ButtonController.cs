using UnityEngine;
using System.Collections;
using System;
using TouchScript.Events;
using TouchScript.Gestures;


[RequireComponent (typeof (ButtonView))]
public class ButtonController : AController {
	
	protected ButtonView buttonView;
	
	protected void Start () {
		buttonView = gameObject.GetComponent<ButtonView>();
        gameObject.AddComponent<PressGesture>();
        GetComponent<PressGesture>().StateChanged += OnPressStateChanged;
	}
	
	public override void OnNotify(NotificationData notificationData){
        GameObject notifingGameObject = notificationData.notifingGameObject;
        if (notifingGameObject == null) return;
        if (notifingGameObject.name.Equals(name))
        {
            if (buttonView != null)
            {
                //Debug.Log("flash " + buttonView);
                buttonView.flash();
            }
        }
    }

    int x = 0;

    private void OnPressStateChanged(object sender, GestureStateChangeEventArgs e)
    {
        
        if ((!gameObject.activeInHierarchy)) return;
        Debug.Log(name + " " + notificationType + " aktiv " + (!gameObject.activeInHierarchy) + " " + GetInstanceID());
        
        if (e.State != Gesture.GestureState.Recognized) return;
        if (applicationDataModel.getBooleanValue(name + "_disabled")) return;

        if (Array.IndexOf(new NotificationType[] { NotificationType.BANKBUTTONCLICKED, NotificationType.PATTERNBUTTONCLICKED, NotificationType.RANGEBUTTONCLICKED, 
		NotificationType.EDITMODEBUTTONCLICKED, NotificationType.PLAYMODEBUTTONCLICKED, NotificationType.BPMCHANGEBUTTONCLICKED }, notificationType) != -1)
        {
            NotifyRootcontroller(new StepControlDataChangeCommand(notificationType, gameObject.name, applicationDataModel));
        }
        else if (Array.IndexOf(new NotificationType[] { NotificationType.STEPBUTTONCKLICKED, NotificationType.FOLLOWSTEPBUTTONCLICKED, NotificationType.SELECTINSTRUMENTEDITMODE, NotificationType.SELECTHARMONY }, notificationType) != -1)
        {
            NotifyRootcontroller(new StepMatrixDataChangeCommand(notificationType, gameObject.name, applicationDataModel));
        }
        else if (Array.IndexOf(new NotificationType[] { NotificationType.OPENSELECTINSTRUMENTCONTAINERBUTTONCLICKED, NotificationType.SELECTTRACKVOLUMEEDITMODE }, notificationType) != -1)
        {
            NotifyRootcontroller(new SoundSelectionDataChangeCommand(notificationType, gameObject.name, applicationDataModel));
        }
        else if (Array.IndexOf(new NotificationType[] { NotificationType.OCTAVESELECTION, NotificationType.PIANOKEYCLICKED  }, notificationType) != -1)
        {
            NotifyRootcontroller(new PianaDataChangeCommand(notificationType, gameObject.name, applicationDataModel));
        }
        else if (Array.IndexOf(new NotificationType[] { NotificationType.SHOWMENU, NotificationType.MENUBUTTONCLICKED, NotificationType.SETUPTYPEBUTTONCLICKED, NotificationType.SELECTTHEMECOLORTYPE, 
            NotificationType.SELECTTHEMECOLOR, NotificationType.RESETCOLOR, NotificationType.SETNEWCOLOR  }, notificationType) != -1)
        {
            NotifyRootcontroller(new MenuDataChangeCommand(notificationType, gameObject.name, applicationDataModel));
        }

        // Matrix aktualisieren:
        if (Array.IndexOf(new NotificationType[] { NotificationType.BANKBUTTONCLICKED, NotificationType.PATTERNBUTTONCLICKED, NotificationType.RANGEBUTTONCLICKED, 
        NotificationType.STEPBUTTONCKLICKED }, notificationType) != -1)
        {
            rootController.OnNotify(new NotificationData(NotificationType.MATRIXDATACHENGED, null, null));            
        }

        changeScreen(notificationType);
    }

    private void changeScreen(NotificationType type){
        if (Array.IndexOf(new NotificationType[] { NotificationType.SELECTINSTRUMENTEDITMODE, NotificationType.MENUBUTTONCLICKED, NotificationType.SETUPTYPEBUTTONCLICKED }, type) != -1)
        {
            ScreenChangingCommand cmd = new ScreenChangingCommand(NotificationType.CHANGESCREEN, null, applicationDataModel, type);
            rootController.OnNotify(new NotificationData(NotificationType.CHANGESCREEN, null, cmd, type));
        }
    }
}
