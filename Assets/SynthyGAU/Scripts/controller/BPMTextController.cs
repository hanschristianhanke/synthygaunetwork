﻿using UnityEngine;
using System.Collections;

public class BPMTextController : AController {
	
	LabelView labelView;
	
	protected void Start () {
		labelView = gameObject.GetComponent<LabelView>();
	}
	
	public override void OnNotify(NotificationData notificationData){
		if (!gameObject.activeInHierarchy) return;
		if (notificationData.notificationType != notificationType) return;
		labelView.setText(""+applicationDataModel.audioDataModel.bpm);
	}
}
