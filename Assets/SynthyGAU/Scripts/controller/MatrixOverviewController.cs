﻿using UnityEngine;
using System.Collections;

using TouchScript.Events;
using TouchScript.Gestures;

public class MatrixOverviewController : AController {

    private Vector2 lp;
    private MatrixOverviewView matrixOverviewView;

    private float lastY = 0;
    private float deltaY = 0;

    private float lastX = 0;
    private float deltaX = 0;

    private bool pan = false;
	private bool busyTransposeVertically = false;
	private bool busyTransposeHorizontally = false;

    void Start(){
        matrixOverviewView = GetComponent<MatrixOverviewView>();
        GetComponent<PanGesture>().StateChanged += OnPanStateChanged;
        GetComponent<ReleaseGesture>().StateChanged += OnReleaseStateChanged;
        GetComponent<PressGesture>().StateChanged += OnPressStateChanged;	
    }

    public override void OnNotify(NotificationData notificationData) {
		matrixOverviewView = matrixOverviewView==null?GetComponent<MatrixOverviewView>():matrixOverviewView;

        NotificationType notificationType = notificationData.notificationType;
        if (audioDataModel.track == -1) return;

        if (notificationType == NotificationType.MATRIXDATACHENGED)
        {
            bool[,] values = audioDataModel.getGetDataFromCurrentTrack().getToneValues();
			matrixOverviewView.redraw(values);
        }
        else if (notificationType == NotificationType.MATRIXBUTTONDATACHANGED)
        {
            Vector2 lp = (Vector2)notificationData.data;
            int x = (int)lp.x;
            int y = (int)lp.y;
            bool[,] values = audioDataModel.getGetDataFromCurrentTrack().getToneValues();
			matrixOverviewView.redraw((audioDataModel.selectedrange *16) + x, (audioDataModel.selectedhight *8) + y, values);
        }
        else if (notificationType == NotificationType.SELECTMATRIXOVERVIEWPART || notificationType == NotificationType.STEPBUTTONCKLICKED)
        {
			matrixOverviewView.setViewFrame(audioDataModel.selectedrange * 16, audioDataModel.selectedhight * 8);
        }
    }

    private void OnPressStateChanged(object sender, GestureStateChangeEventArgs e)
    {
        PressGesture target;
        switch (e.State)
        {
            case Gesture.GestureState.Recognized:
                target = sender as PressGesture;
                if (target.ActiveTouches.Count == 1)
                {
                    TouchScript.TouchPoint tp = target.ActiveTouches[0];
                    lp = Camera.main.ScreenToWorldPoint(tp.Position);
                    lp.x = Mathf.FloorToInt(Mathf.Abs((lp.x) - transform.position.x) / (renderer.bounds.size.x / 4)) * 16;
				lp.y = Mathf.FloorToInt(Mathf.Abs((lp.y + GetComponent<MatrixOverviewView>().offsetY) - transform.position.y) / ((renderer.bounds.size.y - 2 * GetComponent<MatrixOverviewView>().offsetY) / ((6 * audioDataModel.harmonyLength) / 8))) * 8;
                }
                break;
        }
    }

    private void OnReleaseStateChanged(object sender, GestureStateChangeEventArgs e)
    {
        ReleaseGesture target;
        switch (e.State)
        {
            case Gesture.GestureState.Recognized:
				if (!pan){
	                NotifyRootcontroller(new StepMatrixDataChangeCommand(notificationType, name, applicationDataModel, lp), lp);
	                rootController.OnNotify(new NotificationData(NotificationType.STEPBUTTONCKLICKED, null, null));
	                rootController.OnNotify(new NotificationData(NotificationType.MATRIXDATACHENGED, null, null));
				}
                break;
        }
    }

    private void OnPanStateChanged(object sender, GestureStateChangeEventArgs e)
    {
        string output = "";
        PanGesture target = sender as PanGesture;

        switch (e.State)
        {
            case Gesture.GestureState.Began:
                //Debug.Log("pan begin");
                //if (target.ActiveTouches.Count == 2){
                pan = true;
                lastY = 0;
                lastX = 0;
                foreach (TouchScript.TouchPoint tp in target.ActiveTouches)
                {
                    lastY += tp.Position.y;
                    lastX += tp.Position.x;
                }
				lastY = (lastY / target.ActiveTouches.Count);
                deltaY = 0;

				lastX = (lastX / target.ActiveTouches.Count);
                deltaX = 0;
                //	}

                break;

            case Gesture.GestureState.Changed:
                int i = 0;
                //Debug.Log ("pan change");
                float avgY = 0;
                float avgX = 0;

                avgY = 0;
                avgX = 0;
                foreach (TouchScript.TouchPoint tp in target.ActiveTouches)
                {
                    avgY += tp.Position.y;
                    avgX += tp.Position.x;
                }
                avgY = (avgY / target.ActiveTouches.Count);
                avgX = (avgX / target.ActiveTouches.Count);


                deltaY += (avgY - lastY) / 50;
                lastY = avgY;

                deltaX += (avgX - lastX) / 50;
                lastX = avgX;
			if (!busyTransposeHorizontally && !busyTransposeVertically){
			if (Mathf.Abs(deltaY) >= 1)
                {
				busyTransposeVertically = true;
                    deltaY = Mathf.Clamp(Mathf.RoundToInt(deltaY), -1, 1);
                    // MESSAGE HIER EINSETZEN !!!! --> deltaY
                    // Transponieren
                    lp = new Vector2((float)deltaY,0);
					deltaY = 0;
                    rootController.OnNotify(new NotificationData(NotificationType.WIPEMATRIXOVERVIEWVERTICALLY, gameObject, new StepMatrixDataChangeCommand(NotificationType.WIPEMATRIXOVERVIEWVERTICALLY, name, applicationDataModel, lp), lp));
                    rootController.OnNotify(new NotificationData(NotificationType.MATRIXDATACHENGED, null, null));
				busyTransposeVertically = false;
                }

                if (Mathf.Abs(deltaX) >= 1)
                {
				busyTransposeHorizontally = true;
                    deltaX = Mathf.Clamp(Mathf.RoundToInt(deltaX), -1, 1);

                    lp = new Vector2((float)deltaX, 0);
					//lp = new Vector2(0,0);
					deltaX = 0;
                    rootController.OnNotify(new NotificationData(NotificationType.WIPEMATRIXOVERVIEWHORIZONTALLY, gameObject, new StepMatrixDataChangeCommand(NotificationType.WIPEMATRIXOVERVIEWHORIZONTALLY, name, applicationDataModel, lp), lp));
                    rootController.OnNotify(new NotificationData(NotificationType.MATRIXDATACHENGED, null, null));
				busyTransposeHorizontally = false;
                    
                }
			}
                break;

            case Gesture.GestureState.Ended:
                //Debug.Log("pan end");
                pan = false;
                break;
        }
        //debugText.text = output;
    }


}
