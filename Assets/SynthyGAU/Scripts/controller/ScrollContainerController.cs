﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using TouchScript.Events;
using TouchScript.Gestures;

public class ScrollContainerController : AController {

    private ScrollContainerView scrollContainerView;

    void Start()
    {
        GetComponent<PressGesture>().StateChanged += OnPressStateChanged;
        scrollContainerView = GetComponent<ScrollContainerView>();
    }
	
	public override void OnNotify(NotificationData notificationData){}

    private void OnPressStateChanged(object sender, GestureStateChangeEventArgs e)
    {
        PressGesture target;
        switch (e.State)
        {
            case Gesture.GestureState.Recognized:
                List<GameObject> rowEntries = scrollContainerView.rowEntries;
                target = sender as PressGesture;
                object captionObject = null;
                foreach (TouchScript.TouchPoint tp in target.ActiveTouches)
                {
                    Vector2 lp = Camera.main.ScreenToWorldPoint(tp.Position);
                    lp.x = Mathf.FloorToInt(Mathf.Abs(lp.x - transform.position.x) / (rowEntries[0].renderer.bounds.size.x + 1));
                    lp.y = Mathf.FloorToInt(Mathf.Abs(lp.y - transform.position.y) / (rowEntries[0].renderer.bounds.size.y + 1));
                    
                    //eigendlich kacke, aber mir fällt gerade nix besseres ein...
                    captionObject = scrollContainerView.setButtonActive((int)lp.x, (int)lp.y);
                }

                NotifyRootcontroller(new SoundSelectionDataChangeCommand(notificationType, name, applicationDataModel, captionObject), captionObject);
                break;
        }
    }
}
