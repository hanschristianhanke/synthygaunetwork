/*---------------------------------------------------------------------------------
Configures a camera for easy 1:1 pixel management and placement. A convenient
"content" gameobject is attached to the camera as a place to attach objects to the
camera while still allowing logical 1:1 pixel positioning coordinates. If you
intend to use the content gameobject you should turn off calculatePosition so
you can move the camera around as desired. Note that y position values should be
negative as needed for pixel placement.

Author:	Bob Berkebile
Email:	bobb@pixelplacement.com
---------------------------------------------------------------------------------*/

using UnityEngine;
using System.Collections;

//[ ExecuteInEditMode ]
public class SynthyGAUCamera : MonoBehaviour {
	

	Transform content;
	Transform cachedTransform;
	Camera cachedCamera;

	GameObject cornerUpperLeft;
	GameObject cornerLowerRight;

	int previousScreenWidth;
	int previousScreenHeight;

	float fieldOfView = 163f;
	bool done = false;
	
	//-----------------------------------------------------------------------------
	// Init
	//-----------------------------------------------------------------------------
	
	void Awake(){
		cachedTransform = transform;
		cachedCamera = camera;	
		cachedCamera.isOrthoGraphic = false;
		cachedCamera.nearClipPlane = 0.01f;
		cachedCamera.farClipPlane = 1000;
		cachedCamera.fieldOfView = 163.7513f;

		/*
		cornerUpperLeft = (GameObject)Instantiate (Resources.Load ("Components/StepButton"));
		cornerUpperLeft.name = "cornerUpperLeft";
		cornerLowerRight = (GameObject)Instantiate (Resources.Load ("Components/StepButton"));
		cornerLowerRight.name = "cornerLowerRight";

		cornerUpperLeft.transform.localScale = new Vector3 (1, 1, 1);
		cornerLowerRight.transform.localScale = new Vector3 (1, 1, 1);

		cornerUpperLeft.transform.position = new Vector3 (0, 0, 0f);
		cornerLowerRight.transform.position = new Vector3 (2047, -1535, 0f);*/

		//set up child content holder ( if there was already one from a previous life of the PixelCamera script lets use that instead of creating a new one ):
		/*content = cachedTransform.Find( "Content" );
		if ( content == null ) {
			content = (Transform)new GameObject( "Content" ).GetComponent<Transform>();
		}
		content.parent = cachedTransform;
		Calculate();*/
	}
	
	//-----------------------------------------------------------------------------
	// Update
	//-----------------------------------------------------------------------------
		
	void Update () {
		//gameObject.renderer.isVisible
		/*if ( Time.frameCount % 10 != 0 ) {
			return;
		}
		Calculate();*/

		if (!done){
			cachedCamera.fieldOfView = fieldOfView;
			//Vector3 ll = cachedCamera.Vie(new Vector3(1,1,0));
			//Vector3 ur = cachedCamera.ScreenToWorldPoint(new Vector3(Screen.width,Screen.height,0));
			Debug.Log ("   "+ cachedCamera.WorldToScreenPoint(new Vector3(0,0,0)).x);

			if ( cachedCamera.WorldToScreenPoint(new Vector3(0,0,0)).x >= 0){
				done = true;
				//DestroyObject (cornerUpperLeft);
				//DestroyObject (cornerLowerRight);
			} else {
				//Debug.Log ("wrong");
				fieldOfView += 0.001f;

			}
		}
	}	
	
	//-----------------------------------------------------------------------------
	// Private Methods
	//-----------------------------------------------------------------------------
	

}
