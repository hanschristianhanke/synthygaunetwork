﻿using UnityEngine;
using System.Collections;

public class NetworkHandler : AController {
	private PhotonView photonView;

	// Use this for initialization
	void Start () {
		PhotonNetwork.ConnectUsingSettings ("0.1");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public override void OnNotify(NotificationData notificationData){
		}

	void OnPhotonJoinRoomFailed(){
		Debug.Log ("FAILED!!!!");
	}

	void OnCreatedRoom(){
		Debug.Log ("OnCreatedRoom!!!!");
	}

	void OnJoinedRoom(){
		Debug.Log ("OnJoinedRoom!!!!");
	}

	void OnPhotonPlayerConnected(){
		Debug.Log ("OnPhotonPlayerConnected!!!!");
	}

	void OnPhotonPlayerDisconnected(){
		Debug.Log ("OnPhotonPlayerDisconnected!!!!");
	}

	void OnJoinedLobby(){
		RoomOptions roomOptions = new RoomOptions() { isVisible = true, maxPlayers = 4 };
		PhotonNetwork.JoinOrCreateRoom ("test", roomOptions, TypedLobby.Default);
		Debug.Log (PhotonNetwork.GetRoomList ());
		photonView = PhotonView.Get (this);
	}

	public void notifyOthers (ICommand command, object data, string gobj){
		photonView.RPC ("_notifyOthers", PhotonTargets.Others, command.GetType().ToString(), command.getNotificationType(), command.getComponentID(), data, gobj);
		Debug.Log ("da");
	}

	[RPC]
	private void _notifyOthers(string rpctype, string notType, string id, object data, string gobj){
		Debug.Log ("HIER");
		//	NotifyRootcontroller (new Root, data);
		System.Type nodeType = System.Type.GetType(rpctype);
		
		if (rpctype.Equals ("StepMatrixDataChangeCommand")) {
			//AController.notifyOthers(rpctype, notType, id, data);
			AController.NotificationType notificationType = (AController.NotificationType)System.Enum.Parse(typeof( AController.NotificationType),notType);
			NotifyRootcontrollerByNotification(new StepMatrixDataChangeCommand(notificationType,name,applicationDataModel,(Vector2)data),(Vector2)data, GameObject.Find(gobj), notificationType);
		}
		//NotifyRootcontroller(new StepMatrixDataChangeCommand(notificationType,name,applicationDataModel,lp),lp);
		
		//		NotifyRootcontroller (System.Activator(nodeType, new {notType, id, applicationDataModel}), data);
	}

	[RPC]
	private void _notifyOthers(string rpctype, string notType, string id, Vector2 data, string gobj){
		Debug.Log ("HIER "+notType+"   "+(AController.NotificationType)System.Enum.Parse(typeof( AController.NotificationType),notType));
		//	NotifyRootcontroller (new Root, data);
		System.Type nodeType = System.Type.GetType(rpctype);
		
		if (rpctype.Equals ("StepMatrixDataChangeCommand")) {
			//AController.notifyOthers(rpctype, notType, id, data);
			AController.NotificationType notificationType = (AController.NotificationType)System.Enum.Parse(typeof( AController.NotificationType),notType);
			NotifyRootcontrollerByNotification(new StepMatrixDataChangeCommand( notificationType ,id,applicationDataModel,(Vector2)data),(Vector2)data, GameObject.Find(gobj), notificationType);
		}
		//NotifyRootcontroller(new StepMatrixDataChangeCommand(notificationType,name,applicationDataModel,lp),lp);
		
		//		NotifyRootcontroller (System.Activator(nodeType, new {notType, id, applicationDataModel}), data);
	}
}
