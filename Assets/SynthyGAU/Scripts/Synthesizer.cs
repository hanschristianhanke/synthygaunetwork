﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

using CSharpSynth.Effects;
using CSharpSynth.Sequencer;
using CSharpSynth.Synthesis;
using CSharpSynth.Midi;
using System.Collections.Generic;


public class PlayedNote{
	public float ttl = 1;
	public int note;

	public PlayedNote(int pNote){
		note = pNote;
	}

	public bool recalc(){
		ttl *= 0.75f;
		return (ttl) > 0.1f;
	}
}

//[RequireComponent (typeof(AudioSource))]
public class Synthesizer : AController {

	public string midiFilePath = "Midis/mirror_speedeaters2.mid";
	//Try also: "FM Bank/fm" or "Analog Bank/analog" for some different sounds
	public string bankFilePath = "GM Bank/gm";
	public int bufferSize = 1024;
	public int midiNote = 60;
	public int midiNoteVolume = 100;
	public int midiInstrument = 1;
	//Private 
	private float[] sampleBuffer;
	private float gain = 1f;
	private MidiSequencer midiSequencer;
	private StreamSynthesizer midiStreamSynthesizer;
	
	private float sliderValue = 1.0f;
	private float maxSliderValue = 127.0f;

	private List<PlayedNote> playedNotes = new List<PlayedNote> ();
	private ToneColumn[] lastToneColumns = new ToneColumn[8];


	private bool displayGUI = false;
	private int selectedTrack = 0;

	void Update (){

		for (int i = playedNotes.Count - 1; i >= 0; i--)
		{
			if (!playedNotes[i].recalc()){
				midiStreamSynthesizer.NoteOff(10,playedNotes[i].note);
				playedNotes.RemoveAt(i);
			}
		}
	}

    public override void OnNotify(NotificationData notificationData) {
		switch (notificationData.notificationType)
		{
			case NotificationType.PIANOKEYCLICKED: 
			{
						int value = PianoUtil.calculateToneValueForKeybordButton (notificationData.notifingGameObject.name, audioDataModel.firstOctave, audioDataModel.secondOctave);
						//  Debug.Log("piano key played: "+value);
						if (audioDataModel.getTrackInstrument (8) != null) {
								midiStreamSynthesizer.NoteOn (10, value + 24, Mathf.RoundToInt (audioDataModel.pianoVolume * 100), audioDataModel.getTrackInstrument (8).number);
								playedNotes.Add (new PlayedNote (value + 24));
						}
				}
			break;
			/*case NotificationType.SELECTINSTRUMENT:
			{
						int instrumentselection = applicationDataModel.audioDataModel.instrumentselection;

						if (instrumentselection >= 0 && !audioDataModel.tracks[instrumentselection].voiceName.Equals (audioDataModel.getTrackInstrument (instrumentselection).name)) {
								updateTrackInstrument (instrumentselection, audioDataModel.getTrackInstrument (instrumentselection));
						}
				} 
			break;*/
			case NotificationType.MENUBUTTONCLICKED:
			{

				displayGUI = applicationDataModel.selectedmenubutton == 5;

			}
			break;
			/*
		case NotificationType.MATRIXBUTTONDATACHANGED:
		{
			Vector2 lp = (Vector2)notificationData.data;
			int x = (int)lp.x;
			int y = (int)lp.y;
			bool val = audioDataModel.getCurrentToneActive(x, y);

			if (val){
				audioDataModel.tracks[y].addNote(x,1);
			} else {
				audioDataModel.tracks[y].removeNote(x,1);
			}

			Debug.Log ("STEP "+x+" / "+y+" / "+val);
		}
			break;
*/
		case NotificationType.PLAYMODEBUTTONCLICKED:
				if(applicationDataModel.playmode==ApplicationDataModel.Playmode.STOP){
				for (int i = 0; i<audioDataModel.tracks.Length; i++){
						audioDataModel.tracks[i].source.Stop();				
					}
				} else if(applicationDataModel.playmode==ApplicationDataModel.Playmode.PAUSE){
				for (int i = 0; i<audioDataModel.tracks.Length; i++){
						audioDataModel.tracks[i].source.Pause();				
					}
				} else if(applicationDataModel.playmode==ApplicationDataModel.Playmode.PLAY){
				for (int i = 0; i<audioDataModel.tracks.Length; i++){
					audioDataModel.tracks[i].source.Play();				
					}
				}
				break;				
			}		
		}


    public void OnStep(int columnIndex){
//        Debug.Log("step " +  columnIndex);
		//midiStreamSynthesizer.NoteOffAll(true);

//		midiStreamSynthesizer.setPan (1, audioDataModel.mainPan*2-1);

//		float masterVolume = audioDataModel.mainVolume;

//		for (int instrumentIndex = 0; instrumentIndex < 8; instrumentIndex++)
//        {
			/*if (lastToneColumns[instrumentIndex]!=null){
			List<Tone> oldPlayedTones = lastToneColumns[instrumentIndex].playedTones;
			if (oldPlayedTones!=null){

				foreach (Tone oldTone in oldPlayedTones){
						midiStreamSynthesizer.NoteOff (instrumentIndex, (71-oldTone.id)+24);
				}
			}
			}*/

//			for (int oldTone = 24; oldTone< 96; oldTone++){
//				midiStreamSynthesizer.NoteOff (instrumentIndex, oldTone);
//			}


//            Instrument instrument = audioDataModel.getTrackInstrument(instrumentIndex);
//            ToneColumn toneColumn = audioDataModel.getToneColumnFromCurrentTrack(instrumentIndex, columnIndex);
			
//			int volume = Mathf.RoundToInt(audioDataModel.getTackVolume(instrumentIndex) * 100*masterVolume);
				//  Debug.Log(toneColumn);

//			if (toneColumn != null && instrument!=null ){
//				List<Tone> playedTones = toneColumn.playedTones;
//				lastToneColumns[instrumentIndex]=toneColumn;
			/*	for (int i=0 ; i<72; i++){
					midiStreamSynthesizer.NoteOff (1, i+24, 1, instrumentNr);

				}*/

//				int actualChannel = 0;

//	            foreach (Tone tone in playedTones){
	                //instrument.name = name des aktuellen instruments
	                //tone.id = tonhöhe des gerade zu spielenden tones
		//			Debug.Log ("jj "+tone.id);
				//	Debug.Log ("instrument NUmber "+instrument.number);
					//midiStreamSynthesizer.NoteOn (instrumentIndex, (71-tone.id)+24, volume, instrument.number);
//					tracks[instrumentIndex].playNote(1 + ((32-tone.id)/32f));
					//tracks[instrumentIndex].playNote (1f);
//	            }
//			}

//        }
    }

	private void prePitchSubCategory (string categoryName){
		Debug.Log ("PITCH ########### "+categoryName);
		UnityEngine.Object [] objects = Resources.LoadAll("Sounds/"+categoryName);
		List <string> filenames = new List<string>();

		for (int i =0; i<objects.Length; i++){
			filenames.Add(objects[i].name);
			Debug.Log(objects[i].name);
		}

		for (int ii =0; ii<objects.Length; ii++){
			string filename = objects[ii].name;
			if (!filename.Contains("_pitched") && !filenames.Contains(filename+"_pitched")){

				Debug.Log("generate!");
				AudioClip originalClip = (AudioClip)Resources.Load("Sounds/"+categoryName+"/"+filename);
				float [] originalData = new float[originalClip.samples];
				originalClip.GetData(originalData,0);
				
				float [] workingData = new float[originalClip.samples];

				for (int i=0; i<72; i++){
					originalData.CopyTo(workingData,0);
					Debug.Log ("pitching "+categoryName+"/"+filename);
					if (i!=35){
						PitchShifter.PitchShift( Mathf.Pow(2, ( (i-35f)/12f)), originalClip.samples, originalClip.frequency, workingData);
					}
					//var byteArray = new byte[workingData.Length * 4];
					//Buffer.BlockCopy(workingData, 0, byteArray, 0, byteArray.Length);
					//System.IO.File.WriteAllBytes (Application.dataPath + "/Resources/Sounds/Pitched/"+categoryName+"_"+filename+"_"+i+".txt", byteArray);
					originalClip.SetData(workingData,0);
					SavWav.Save(Application.dataPath + "/Resources/Sounds/Pitched/"+categoryName+"_"+filename+"_"+i+".WAV", originalClip);
				}

				System.IO.File.WriteAllText (Application.dataPath + "/Resources/Sounds/"+categoryName+"/"+filename+"_pitched.txt", "done");
				//}
			}
		}
		objects = null;
	}


	void Start(){


	//	prePitchSubCategory ("CR8K");

	//	prePitchSubCategory ("CYMBA");
	//	prePitchSubCategory ("REKKERD");
	//	prePitchSubCategory ("BASEDRUM");
	//	prePitchSubCategory ("UNIVOX");



		/*
		float [] test = new float[100];
		for (int i=0; i<100; i++) {
			test[i] = 99;		
		}

		var byteArray = new byte[test.Length * 4];
		System.Buffer.BlockCopy(test, 0, byteArray, 0, byteArray.Length);

		System.IO.File.WriteAllBytes (Application.dataPath + "/Resources/test.data", byteArray);
		*/
		//midiStreamSynthesizer = new StreamSynthesizer (44100, 2, bufferSize, 40);
		//sampleBuffer = new float[midiStreamSynthesizer.BufferSize];		
		
		//midiStreamSynthesizer.LoadBank (bankFilePath);

		/*
		sources = new AudioSource[8,8];		
		for (int instrumentIndex = 0; instrumentIndex < 8; instrumentIndex++) {
			for (int channelIndex = 0; channelIndex < 8; channelIndex++)
			{	
				AudioSource snd = gameObject.AddComponent<AudioSource>();				
				snd.clip = (AudioClip)Resources.Load("Sounds/Cello C2");
				snd.loop = false;
				snd.playOnAwake = false;
				snd.pitch = 1;
				sources[instrumentIndex, channelIndex] = snd;
				
			}
		}
		*/

		audioDataModel.tracks = new Track[8];

		for (int i=0; i<8; i++) {
			audioDataModel.tracks[i] = (Track)GameObject.Find("Track"+i).GetComponent<Track>();
			audioDataModel.tracks[i].changeVoice((AudioClip)Resources.Load("Sounds/Cello C2"));
		}

		//midiSequencer = new MidiSequencer (midiStreamSynthesizer);
		//midiSequencer.LoadMidi (midiFilePath, false);
		//These will be fired by the midiSequencer when a song plays. Check the console for messages
		//midiSequencer.NoteOnEvent += new MidiSequencer.NoteOnEventHandler (MidiNoteOnHandler);
		//midiSequencer.NoteOffEvent += new MidiSequencer.NoteOffEventHandler (MidiNoteOffHandler);	
		//midiSequencer.Play ();
		//midiStreamSynthesizer.NoteOn (1, 60, 1, 1);
	}
	/*
	private void OnAudioFilterRead (float[] data, int channels)
	{		
		//This uses the Unity specific float method we added to get the buffer
		midiStreamSynthesizer.GetNext (sampleBuffer);
		
		for (int i = 0; i < data.Length; i++) {
			data [i] = sampleBuffer [i] * gain;
		}
	}*/

	public void stopAll(){
		//midiStreamSynthesizer.NoteOffAll(false);
	}



	void OnGUI(){
		if (displayGUI) {

			Track currentTrack = audioDataModel.tracks [selectedTrack];

			int width = 100;
			int height = 20;
			int margin = 15;

			int x = 25;
			int y = 20;
			GUI.Label (new Rect (x, y, width * 1.2f, height), "Sel. Track: " + selectedTrack);
			selectedTrack = (int)GUI.HorizontalSlider (new Rect (x + width, y + height / 4, width * 2, height), selectedTrack, 0, audioDataModel.tracks.Length - 1);

			currentTrack.MutedVoice = GUI.Toggle (new Rect (x + width * 3f + margin, y, margin, margin), currentTrack.MutedVoice, ""); 

			//y = y+ height + margin;

			float t = 0;

			int beginY = y;
			GUI.Box ( new Rect(0,0,10*width, 40*height),"");

			AudioChorusFilter chorus = currentTrack.chorusFilter;

			GUI.Label (new Rect (x, y += height + margin, width * 1.5f, height), "CHORUS-FILTER");
			chorus.enabled = GUI.Toggle (new Rect (x + width * 1.5f + margin, y, margin, margin), chorus.enabled, ""); 
			GUI.Label (new Rect (x, y += height + margin, width * 1.2f, height), "Dry Mix");
			chorus.dryMix = GUI.HorizontalSlider (new Rect (x + width, y + height / 4, width, height), chorus.dryMix, 0, 1);
			GUI.Label (new Rect (x, y += height + margin, width * 1.2f, height), "Wet Mix 1");
			chorus.wetMix1 = GUI.HorizontalSlider (new Rect (x + width, y + height / 4, width, height), chorus.wetMix1, 0, 1);
			GUI.Label (new Rect (x, y += height + margin, width * 1.2f, height), "Wet Mix 2");
			chorus.wetMix2 = GUI.HorizontalSlider (new Rect (x + width, y + height / 4, width, height), chorus.wetMix2, 0, 1);
			GUI.Label (new Rect (x, y += height + margin, width * 1.2f, height), "Wet Mix 3");
			chorus.wetMix3 = GUI.HorizontalSlider (new Rect (x + width, y + height / 4, width, height), chorus.wetMix3, 0, 1);
			GUI.Label (new Rect (x, y += height + margin, width * 1.2f, height), "Delay");
			chorus.delay = GUI.HorizontalSlider (new Rect (x + width, y + height / 4, width, height), chorus.delay, 0, 100);
			GUI.Label (new Rect (x, y += height + margin, width * 1.2f, height), "Rate");
			chorus.rate = GUI.HorizontalSlider (new Rect (x + width, y + height / 4, width, height), chorus.rate, 0, 100);
			GUI.Label (new Rect (x, y += height + margin, width * 1.2f, height), "Depth");
			chorus.depth = GUI.HorizontalSlider (new Rect (x + width, y + height / 4, width, height), chorus.depth, 0, 1);

			GUI.Label (new Rect (x, y += height + margin * 2, width * 1.5f, height), "DISORTION-FILTER");
			currentTrack.disortationFilter.enabled = GUI.Toggle (new Rect (x + width * 1.5f + margin, y, margin, margin), currentTrack.disortationFilter.enabled, ""); 
			GUI.Label (new Rect (x, y += height + margin, width * 1.2f, height), "Level");
			currentTrack.disortationFilter.distortionLevel = GUI.HorizontalSlider (new Rect (x + width, y + height / 4, width, height), currentTrack.disortationFilter.distortionLevel, 0, 1);


			AudioEchoFilter echo = currentTrack.echoFilter;

			GUI.Label (new Rect (x, y += height + margin * 2, width * 1.5f, height), "ECHO-FILTER");
			echo.enabled = GUI.Toggle (new Rect (x + width * 1.5f + margin, y, margin, margin), echo.enabled, ""); 
			GUI.Label (new Rect (x, y += height + margin, width * 1.2f, height), "Delay");
			echo.delay = (int)GUI.HorizontalSlider (new Rect (x + width, y + height / 4, width, height), echo.delay, 10, 5000);
			GUI.Label (new Rect (x, y += height + margin, width * 1.2f, height), "Decay");
			echo.decayRatio = GUI.HorizontalSlider (new Rect (x + width, y + height / 4, width, height), echo.decayRatio, 0, 1);
			GUI.Label (new Rect (x, y += height + margin, width * 1.2f, height), "Wet Mix");
			echo.wetMix = GUI.HorizontalSlider (new Rect (x + width, y + height / 4, width, height), echo.wetMix, 0, 1);
			GUI.Label (new Rect (x, y += height + margin, width * 1.2f, height), "Dry Mix");
			echo.dryMix = GUI.HorizontalSlider (new Rect (x + width, y + height / 4, width, height), echo.dryMix, 0, 1);

			x = (int)(x + 2 * width * 1.2f + margin);
			y = beginY;

			GUI.Label (new Rect (x, y += height + margin, width * 1.5f, height), "HP-FILTER");
			currentTrack.highPassFilter.enabled = GUI.Toggle (new Rect (x + width * 1.5f + margin, y, margin, margin), currentTrack.highPassFilter.enabled, ""); 
			GUI.Label (new Rect (x, y += height + margin, width * 1.2f, height), "Cut-Off");
			currentTrack.highPassFilter.cutoffFrequency = (int)GUI.HorizontalSlider (new Rect (x + width, y + height / 4, width, height), currentTrack.highPassFilter.cutoffFrequency, 10, 50000);
			GUI.Label (new Rect (x, y += height + margin, width * 1.2f, height), "HP-Res.");
			currentTrack.highPassFilter.highpassResonaceQ = GUI.HorizontalSlider (new Rect (x + width, y + height / 4, width, height), currentTrack.highPassFilter.highpassResonaceQ, 1, 10);

			GUI.Label (new Rect (x, y += height + margin * 2, width * 1.5f, height), "LP-FILTER");
			currentTrack.lowPassFilter.enabled = GUI.Toggle (new Rect (x + width * 1.5f + margin, y, margin, margin), currentTrack.lowPassFilter.enabled, ""); 
			GUI.Label (new Rect (x, y += height + margin, width * 1.2f, height), "Cut-Off");
			currentTrack.lowPassFilter.cutoffFrequency = (int)GUI.HorizontalSlider (new Rect (x + width, y + height / 4, width, height), currentTrack.lowPassFilter.cutoffFrequency, 10, 50000);
			GUI.Label (new Rect (x, y += height + margin, width * 1.2f, height), "LP-Res.");
			currentTrack.lowPassFilter.lowpassResonaceQ = GUI.HorizontalSlider (new Rect (x + width, y + height / 4, width, height), currentTrack.lowPassFilter.lowpassResonaceQ, 1, 10);

			x = (int)(x + 2 * width * 1.2f + margin);
			y = beginY;

			AudioReverbFilter reverb = currentTrack.reverbFilter;

			GUI.Label (new Rect (x, y += height + margin, width * 1.5f, height), "REVERB-FILTER");
			reverb.enabled = GUI.Toggle (new Rect (x + width * 1.5f + margin, y, margin, margin), reverb.enabled, ""); 
			GUI.Label (new Rect (x, y += height + margin, width * 1.2f, height), "Dry Level");
			reverb.dryLevel = (int)GUI.HorizontalSlider (new Rect (x + width, y + height / 4, width, height), reverb.dryLevel, -10000, 0);
			GUI.Label (new Rect (x, y += height + margin, width * 1.2f, height), "Room");
			reverb.room = (int)GUI.HorizontalSlider (new Rect (x + width, y + height / 4, width, height), reverb.room, -10000, 0);
			GUI.Label (new Rect (x, y += height + margin, width * 1.2f, height), "Room HF");
			reverb.roomHF = (int)GUI.HorizontalSlider (new Rect (x + width, y + height / 4, width, height), reverb.roomHF, -10000, 0);
			GUI.Label (new Rect (x, y += height + margin, width * 1.2f, height), "Room LF");
			reverb.roomLF = (int)GUI.HorizontalSlider (new Rect (x + width, y + height / 4, width, height), reverb.roomLF, -10000, 0);
			GUI.Label (new Rect (x, y += height + margin, width * 1.2f, height), "Decay T");
			reverb.decayTime = GUI.HorizontalSlider (new Rect (x + width, y + height / 4, width, height), reverb.decayTime, 0.1f, 20f);
			GUI.Label (new Rect (x, y += height + margin, width * 1.2f, height), "Decay HF");
			reverb.decayHFRatio = GUI.HorizontalSlider (new Rect (x + width, y + height / 4, width, height), reverb.decayHFRatio, 0.1f, 2f);
			GUI.Label (new Rect (x, y += height + margin, width * 1.2f, height), "Refl Lvl");
			reverb.reverbLevel = (int)GUI.HorizontalSlider (new Rect (x + width, y + height / 4, width, height), reverb.reverbLevel, -10000, 1000);
			GUI.Label (new Rect (x, y += height + margin, width * 1.2f, height), "Refl Del");
			reverb.reflectionsLevel = GUI.HorizontalSlider (new Rect (x + width, y + height / 4, width, height), reverb.reflectionsLevel, 0, 0.3f);
			GUI.Label (new Rect (x, y += height + margin, width * 1.2f, height), "HF Ref");
			reverb.hfReference = (int)GUI.HorizontalSlider (new Rect (x + width, y + height / 4, width, height), reverb.hfReference, 1000, 20000);
			GUI.Label (new Rect (x, y += height + margin, width * 1.2f, height), "LF Ref");
			reverb.lFReference = (int)GUI.HorizontalSlider (new Rect (x + width, y + height / 4, width, height), reverb.lFReference, 20, 1000);
			GUI.Label (new Rect (x, y += height + margin, width * 1.2f, height), "Diff");
			reverb.diffusion = GUI.HorizontalSlider (new Rect (x + width, y + height / 4, width, height), reverb.diffusion, 0, 100f);
			GUI.Label (new Rect (x, y += height + margin, width * 1.2f, height), "Density");
			reverb.density = GUI.HorizontalSlider (new Rect (x + width, y + height / 4, width, height), reverb.density, 0, 100f);

		}
	}

}
